
  case(80)
    !  80 (80)   10  30  4
    S0(ix) = -1d0
    if (level > 1) then
      S1(3,ix) = -2.5d0
      S1(6,ix) = 7.5d0
      S1(15,ix) = 1.5d0
      if (level > 2) then
        S2(6,3,ix) = 11.25d0
        S2(6,6,ix) = -22.5d0
        S2(15,6,ix) = -3.75d0
      end if
    end if

  case(81)
    !  81 (81)   10  31c 4
    if (level > 1) then
      S1(4,ix) = (5d0*Sqrt(1.5d0))/2.d0
      S1(13,ix) = Sqrt(1.5d0)/2.d0
      if (level > 2) then
        S2(4,3,ix) = (15d0*Sqrt(1.5d0))/4.d0
        S2(6,4,ix) = (-15d0*Sqrt(1.5d0))/2.d0
        S2(13,6,ix) = (-5d0*Sqrt(1.5d0))/4.d0
        S2(15,4,ix) = (-5d0*Sqrt(1.5d0))/4.d0
      end if
    end if

  case(82)
    !  82 (82)   10  31s 4
    if (level > 1) then
      S1(5,ix) = (5d0*Sqrt(1.5d0))/2.d0
      S1(14,ix) = Sqrt(1.5d0)/2.d0
      if (level > 2) then
        S2(5,3,ix) = (15d0*Sqrt(1.5d0))/4.d0
        S2(6,5,ix) = (-15d0*Sqrt(1.5d0))/2.d0
        S2(14,6,ix) = (-5d0*Sqrt(1.5d0))/4.d0
        S2(15,5,ix) = (-5d0*Sqrt(1.5d0))/4.d0
      end if
    end if

  case(83)
    !  83 (83)   10  32c 4
    if (level > 1) then
      if (level > 2) then
        S2(4,4,ix) = (-3d0*rt15)/2.d0
        S2(5,5,ix) = (3d0*rt15)/2.d0
        S2(13,4,ix) = -rt15/4.d0
        S2(14,5,ix) = rt15/4.d0
      end if
    end if

  case(84)
    !  84 (84)   10  32s 4
    if (level > 1) then
      if (level > 2) then
        S2(5,4,ix) = (-3d0*rt15)/2.d0
        S2(13,5,ix) = -rt15/4.d0
        S2(14,4,ix) = -rt15/4.d0
      end if
    end if

  case(85)
    !  85 (85)   10  33c 4
    if (level > 1) then
      if (level > 2) then
      end if
    end if

  case(86)
    !  86 (86)   10  33s 4
    if (level > 1) then
      if (level > 2) then
      end if
    end if

  case(87)
    !  87 (87)   11c 30  4
    if (level > 1) then
      S1(1,ix) = -2.5d0
      S1(9,ix) = 1.5d0
      if (level > 2) then
        S2(6,1,ix) = 11.25d0
        S2(9,6,ix) = -3.75d0
      end if
    end if

  case(88)
    !  88 (88)   11c 31c 4
    S0(ix) = Sqrt(1.5d0)/2.d0
    if (level > 1) then
      S1(6,ix) = (-5d0*Sqrt(1.5d0))/4.d0
      S1(7,ix) = Sqrt(1.5d0)/2.d0
      if (level > 2) then
        S2(4,1,ix) = (15d0*Sqrt(1.5d0))/4.d0
        S2(6,6,ix) = (5d0*Sqrt(1.5d0))/4.d0
        S2(7,6,ix) = (-5d0*Sqrt(1.5d0))/4.d0
        S2(9,4,ix) = (-5d0*Sqrt(1.5d0))/4.d0
      end if
    end if

  case(89)
    !  89 (89)   11c 31s 4
    if (level > 1) then
      S1(8,ix) = Sqrt(1.5d0)/2.d0
      if (level > 2) then
        S2(5,1,ix) = (15d0*Sqrt(1.5d0))/4.d0
        S2(8,6,ix) = (-5d0*Sqrt(1.5d0))/4.d0
        S2(9,5,ix) = (-5d0*Sqrt(1.5d0))/4.d0
      end if
    end if

  case(90)
    !  90 (90)   11c 32c 4
    if (level > 1) then
      S1(4,ix) = -rt15/4.d0
      if (level > 2) then
        S2(6,4,ix) = rt15/4.d0
        S2(7,4,ix) = -rt15/4.d0
        S2(8,5,ix) = rt15/4.d0
      end if
    end if

  case(91)
    !  91 (91)   11c 32s 4
    if (level > 1) then
      S1(5,ix) = -rt15/4.d0
      if (level > 2) then
        S2(6,5,ix) = rt15/4.d0
        S2(7,5,ix) = -rt15/4.d0
        S2(8,4,ix) = -rt15/4.d0
      end if
    end if

  case(92)
    !  92 (92)   11c 33c 4
    if (level > 1) then
      if (level > 2) then
        S2(4,4,ix) = (3d0*Sqrt(2.5d0))/4.d0
        S2(5,5,ix) = (-3d0*Sqrt(2.5d0))/4.d0
      end if
    end if

  case(93)
    !  93 (93)   11c 33s 4
    if (level > 1) then
      if (level > 2) then
        S2(5,4,ix) = (3d0*Sqrt(2.5d0))/4.d0
      end if
    end if

  case(94)
    !  94 (94)   11s 30  4
    if (level > 1) then
      S1(2,ix) = -2.5d0
      S1(12,ix) = 1.5d0
      if (level > 2) then
        S2(6,2,ix) = 11.25d0
        S2(12,6,ix) = -3.75d0
      end if
    end if

  case(95)
    !  95 (95)   11s 31c 4
    if (level > 1) then
      S1(10,ix) = Sqrt(1.5d0)/2.d0
      if (level > 2) then
        S2(4,2,ix) = (15d0*Sqrt(1.5d0))/4.d0
        S2(10,6,ix) = (-5d0*Sqrt(1.5d0))/4.d0
        S2(12,4,ix) = (-5d0*Sqrt(1.5d0))/4.d0
      end if
    end if

  case(96)
    !  96 (96)   11s 31s 4
    S0(ix) = Sqrt(1.5d0)/2.d0
    if (level > 1) then
      S1(6,ix) = (-5d0*Sqrt(1.5d0))/4.d0
      S1(11,ix) = Sqrt(1.5d0)/2.d0
      if (level > 2) then
        S2(5,2,ix) = (15d0*Sqrt(1.5d0))/4.d0
        S2(6,6,ix) = (5d0*Sqrt(1.5d0))/4.d0
        S2(11,6,ix) = (-5d0*Sqrt(1.5d0))/4.d0
        S2(12,5,ix) = (-5d0*Sqrt(1.5d0))/4.d0
      end if
    end if

  case(97)
    !  97 (97)   11s 32c 4
    if (level > 1) then
      S1(5,ix) = rt15/4.d0
      if (level > 2) then
        S2(6,5,ix) = -rt15/4.d0
        S2(10,4,ix) = -rt15/4.d0
        S2(11,5,ix) = rt15/4.d0
      end if
    end if

  case(98)
    !  98 (98)   11s 32s 4
    if (level > 1) then
      S1(4,ix) = -rt15/4.d0
      if (level > 2) then
        S2(6,4,ix) = rt15/4.d0
        S2(10,5,ix) = -rt15/4.d0
        S2(11,4,ix) = -rt15/4.d0
      end if
    end if

  case(99)
    !  99 (99)   11s 33c 4
    if (level > 1) then
      if (level > 2) then
        S2(5,4,ix) = (-3d0*Sqrt(2.5d0))/4.d0
      end if
    end if

  case(100)
    !  100 (100)   11s 33s 4
    if (level > 1) then
      if (level > 2) then
        S2(4,4,ix) = (3d0*Sqrt(2.5d0))/4.d0
        S2(5,5,ix) = (-3d0*Sqrt(2.5d0))/4.d0
      end if
    end if
