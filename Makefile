#  This Makefile is designed for use with Gnu Make, and probably won't
#  work with the standard versions of Make.

#  BASE is your main Orient program directory
BASE     := ${shell pwd}
#  If the above doesn't work, use something like this:
#  BASE   := ${PWD}
#  or
#  BASE   := ${HOME}/Orient-${VERSION}

#  Specify your machine architecture here, or set the
#  environment variable ARCH.
ifndef ARCH
ARCH    := x86-64
#  ARCH    := osx
endif

#  Select the default compiler here, or set the
#  environment variable COMPILER.
ifndef COMPILER
COMPILER := gfortran
#  COMPILER := nag
#  COMPILER := ifort
endif

#  Use 
#  make OPENGL=no
#  to over-ride this, or change it here.
OPENGL := yes

#  See ${BASE}/${ARCH}/${COMPILER}/Flags for compiler options.
#  Check the settings in this file.
include $(BASE)/${ARCH}/$(COMPILER)/Flags

#  Use
#    make DEBUG="-g"
#  (for example) to make a version for debugging
ifdef DEBUG
  DIR := ${ARCH}/$(COMPILER)/debug
  FFLAGS := ${FFLAGS} ${DEBUG}
else
  DIR := ${ARCH}/$(COMPILER)/exe
endif

#  Choose where you want to put the installed binary. The normal make
#  leaves a a link to the binary from ${BASE}/bin anyway. 
PREFIX  := /usr/local
INSTALL_DIR := ${PREFIX}/bin

RECIPES = /usr/local/numerical-recipes/recipes_f/recipes/

#  It shouldn't be necessary to change anything below this line
#--------------------------------------------------------------

.SUFFIXES:

SRC := ${BASE}/src

vpath %.f      ${SRC}
vpath %.f90    ${SRC}
vpath %.F90    ${SRC}
vpath %.strip  ${SRC}
vpath %.x90    ${SRC}

include $(BASE)/${ARCH}/$(COMPILER)/Flags

include ${BASE}/VERSION
DATE   :=$(shell date '+%d %b %Y')

.SUFFIXES:
include ${SRC}/Object-list
include ${SRC}/Module-list


MODULES := ${subst .F90,.o,${subst .f90,.o,${MODULESRC}}}
OBJECTS := ${subst .F90,.o,${subst .f,.o,${subst .f90,.o,${SOURCEFILES}}}}

ifeq "${strip ${OPENGL}}" "yes"
  GLMOD = f03gl_gl.o f03gl_glu.o f03gl_glut.o \
          marching_cubes.o gl_module.o display.o
  IGNORE := --ignore f03gl_kinds
else
  IGNORE := --ignore display
  NG := -ng
endif


orient:
	mkdir -p ${BASE}/${DIR}; cd ${BASE}/${DIR}; ${MAKE} -f ${BASE}/Makefile orient-${VERSION}.${PATCHLEVEL}${NG} BASE=${BASE}
	cd ${BASE}/${DIR}; ln -sf orient-${VERSION}.${PATCHLEVEL}${NG} orient
	cd ${BASE}/bin; ln -sf ../${DIR}/orient .

orient-${VERSION}.${PATCHLEVEL}${NG}: modules objects
	@echo PWD = ${PWD}
	${FC} -o orient-${VERSION}.${PATCHLEVEL}${NG} ${LIBRARIES} \
            ${LDFLAGS} ${MODULES} ${OBJECTS} ${GLMOD} ${LIBS}

modules: ${MODULES} ${GLMOD}

objects: modules ${OBJECTS}

#  To run tests, use
#  make tests [TESTS="tests to run"] [ARGS=--force]
#  Default is to run all tests, except that tests already done
#  successfully are not repeated unless ARGS=--force is specified.
#  Use
#  make tests ARGS=--help
#  for more details, or change to the tests directory and run
#  orient_tests.py --help
test tests: force
	cd tests; ./orient_tests.py ${ARGS} ${TEST} ${TESTS}
#	cd tests; rm -f test.log; ${MAKE} BASE=${BASE} ARCH=${ARCH} \
#            COMPILER=${COMPILER} DEBUG="${DEBUG}" \
#            TEST="${TESTS}${TEST}" FRC=${FRC}

install: ${INSTALL_DIR}/orient

${INSTALL_DIR}/orient: ${BASE}/${DIR}/orient
	cp -p ${BASE}/${DIR}/orient ${INSTALL_DIR}

clean:
	cd ${DIR}; rm *.mod *.o

clean_tests:
	cd tests; ./orient_tests.py --clean

force:


%.o: %.F90
	$(FC) $(FFLAGS) $(DEFS) -c $< -o $@

%.o: %.f90
	$(FC) $(FFLAGS) -c $< -o $@

%.o: %.f
	$(FC) $(FFLAGS) -c $< -o $@

%.mod: %.f90
	$(FC) $(FFLAGS) -c $<

%.mod: %.F90
	$(FC) $(FFLAGS) -c $<

include ${BASE}/src/Dependencies

ifeq "${OPENGL}" "yes"
orient.o: display.mod
orient.o: orient.F90 force
	${FC} ${FFLAGS} -DOGL -c $< -o $@
else
orient.o: orient.F90 force
	${FC} ${FFLAGS} -c $< -o $@
endif

#  Special cases:
version.mod:  force
	${BASE}/bin/version.py ${BASE}/VERSION ${BASE}/src/version.f90 ${COMPILER}
	$(FC) $(FFLAGS2) $(DEFS) -c ${BASE}/src/version.f90

patch: src/recipes.f90 src/virial_module.F90 src/md.F90 src/histograms.f90

virial_module.F90 md.F90: %.F90: %.strip
	cd ${SRC}; ${BASE}/bin/patch_recipes.pl --recipes ${RECIPES} $@

histograms.f90: %.f90: %.strip
	cd ${SRC}; ${BASE}/bin/patch_recipes.pl --recipes ${RECIPES} $@

recipes.f90:
	cd ${SRC}; ${BASE}/bin/patch_recipes.pl --recipes ${RECIPES} $@

