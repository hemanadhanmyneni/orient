\documentclass[12pt,mathptmx]{paper}

\usepackage{preamble}
\usepackage{natbib}
\usepackage{array}

\DeclareMathSymbol{\ba}{\mathord}{mathbold}{`a}
\DeclareMathSymbol{\bb}{\mathord}{mathbold}{`b}
\DeclareMathSymbol{\bn}{\mathord}{mathbold}{`n}
\DeclareMathSymbol{\bp}{\mathord}{mathbold}{`p}
\DeclareMathSymbol{\bq}{\mathord}{mathbold}{`q}
\DeclareMathSymbol{\bw}{\mathord}{mathbold}{`w}
\DeclareMathSymbol{\bx}{\mathord}{mathbold}{`x}
\DeclareMathSymbol{\by}{\mathord}{mathbold}{`y}
\DeclareMathSymbol{\bz}{\mathord}{mathbold}{`z}
\DeclareMathSymbol{\bA}{\mathord}{mathbold}{`A}
\DeclareMathSymbol{\bB}{\mathord}{mathbold}{`B}
\DeclareMathSymbol{\bD}{\mathord}{mathbold}{`D}
\DeclareMathSymbol{\bI}{\mathord}{mathbold}{`I}
\DeclareMathSymbol{\bM}{\mathord}{mathbold}{`M}
\DeclareMathSymbol{\bN}{\mathord}{mathbold}{`N}
\DeclareMathSymbol{\bR}{\mathord}{mathbold}{`R}
\DeclareMathSymbol{\bW}{\mathord}{mathbold}{`W}

% \newcolumntype{L}{>{$}l<{$}}
% \newcolumntype{R}{>{$}r<{$}}
% \newcolumntype{C}{>{$}c<{$}}

\newcommand\nhat{\hat n}
\newcommand\bnhat{\hat \bN}
\newcommand\Rhat{\hat\bR}

\bibliographystyle{timf}

\begin{document}

\begin{center}
\large\textbf{Implementation Notes for the Orient program}
\hfill\now\quad\today
  \end{center}

\section{Evaluation of derivatives wwith respect to geometrical coordinates}

\subsection{Derivatives w.r.t. position}

We consider a pair of molecules $A$
and $B$, with centres of mass at $\bA$ and $\bB$, and specifically the
interaction between the multipole moments on site $a$, at position
$\bA+\ba$, and site $b$, at position $\bB+\bb$. As molecule $A$
rotates, the local position vector $\ba$ rotates with it,
and we have to take this into account as well as the rotation of the
multipoles on each site. We need to evaluate the derivatives of the
inter-site distance $R$ and the 15
direction cosines that we use to express
the $S$ functions. They are:
\begin{equation*}
\begin{tabular}{RRRR}
\bx^a \cdot \bx^b,& \bx^a \cdot \by^b,& \bx^a \cdot \bz^b,& \bx^a \cdot \Rhat,\\
\by^a \cdot \bx^b,& \by^a \cdot \by^b,& \by^a \cdot \bz^b,& \by^a \cdot \Rhat,\\
\bz^a \cdot \bx^b,& \bz^a \cdot \by^b,& \bz^a \cdot \bz^b,& \bz^a \cdot \Rhat,\\
-\Rhat \cdot \bx^b,& -\Rhat \cdot \by^b,& -\Rhat \cdot \bz^b,\label{mo:cos}
\end{tabular}
\end{equation*}
where $\Rhat$ is the unit vector in the direction of the intersite
vector $\bR=\bB+\bb-\bA-\ba$. The minus sign appears in
the last three direction cosines to make the $S$-function formulae
symmetrical --- in effect we use the vector from $a$ to $b$ with the
local axes on $a$, but the vector from $b$ to $a$ with the local axes
on $b$. We denote by $\bM^A$ the rotation matrix describing
the orientation of $A$. That is, $\bM^A$ is the rotation that takes
molecule $A$ to its current orientation from a reference orientation with
its molecular axes parallel to the global axes. It is convenient to write
$\ba=\bM^A(\bM^A)^{-1}\ba \equiv \bM^A\tilde\ba$, so that $\tilde\ba$ is
the position of site $a$ in molecular axes relative to the molecular
origin $\bA$, and is constant. Similarly we can write
$\bb=\bM^B(\bM^B)^{-1}\bb \equiv bM^B\tilde\bb$.
% when we are considering derivatives with 
% respect to the orientation of $B$, but for the present we consider
% only rotational derivatives for $A$.

It is also convenient to define an orthogonal matrix $\bW_a$ whose
columns are the unit vectors $\bx_a$, $\by_a$ and $\bz_a$ defining the
local axes of site $a$ in global coordinates. $\bW_a$ describes the
orientation of the local axes of site $a$ relative to the global axes.
We write $\bW_a=\bM^A\widetilde{\bW}^a$, where
$\widetilde{\bW}^a=(\bM^A)^{-1}\bW_a$ is the orientation of the local axes for
site $a$ relative to the molecular axes, and is unchanged by rotations
of the molecule. Similarly $\bW_b$ describes the
orientation of the local axes of site $b$ relative to the global axes,
and $\widetilde{\bW}^b=(\bM^B)^{-1}\bW_b$ is a constant matrix
describing the orientation of site $b$ relative to the molecular axes
of $B$.

In terms of these matrices, the direction cosines between the local
axes for site $a$ and those for site $b$ are the elements of
$(\bW^a)^T\bW^b$, and $\Rhat_\alpha\bW^a_{\alpha\beta}$ is the
unit vector $\Rhat$ in the local axes for site $a$, that is, the
vector of direction cosines $(\bx^a\cdot\Rhat, \by^a\cdot\Rhat, \by^a\cdot\Rhat)$.

In this notation, the derivatives with respect to the position of $A$
are easily obtained:
\begin{align}
&\dbyd{A_\alpha} R^2 = \dbyd{A_\alpha} |\bB+\bb-\bA-\ba|^2 =
-2(B_\alpha+b_\alpha-A_\alpha-a_\alpha)=-2R_\alpha,\notag\\
&\dbyd[R]{A_\alpha} = \half (R^2)^{-1/2} \dbyd[R^2]{A_\alpha}
     = -\frac{R_\alpha}{R},\\
&\dbyd{A_\alpha} R_\beta W^s_{\beta\gamma} = -W^s_{\alpha\gamma},
  \quad\text{$s=a$ or $b$},\notag\\
&\dbyd{A_\alpha} \Rhat_\beta W^s_{\beta\gamma}
    = \dbyd{A_\alpha} \biggl(\frac{R_\beta}{R} W^s_{\beta\gamma}\biggr)
    = -\frac{1}{R}W^s_{\alpha\gamma}
      + \frac{R_\alpha R_\beta}{R^3} W^s_{\beta\gamma},
  \quad\text{$s=a$ or $b$},\notag\\
&\dbyd{A_\alpha} (\bW^a)^{-1}\bW^b = 0.
\end{align}

Similar formulae apply for the derivatives with respect to the
position of $B$, but there are some sign changes:
\begin{align}
&\dbyd[R]{B_\alpha}=+\frac{R_\alpha}{R},\notag\\
&\dbyd{B_\alpha} \Rhat_\beta W^s_{\beta\gamma}
    = +\frac{1}{R}W^s_{\alpha\gamma}
      - \frac{R_\alpha R_\beta}{R^3} W^s_{\beta\gamma},
  \quad\text{$s=a$ or $b$}.\notag
\end{align}

\subsection{Derivatives w.r.t. orientation}

The orientation of a molecule can be described in a number of ways, as
explained in the Introduction.
% in \Sec\ref{intro:coords} on p.~\pageref{intro:coords}.
For the moment we assume that the matrix $\bM^A$ describing the orientation
of $A$ is expressed in terms of some set of orientation coordinates
$p^A_k$, and that we can evaluate the derivatives
\begin{equation}
\bM^A_k = \dbyd[M^A]{p_k} \text{ and }
\bM^A_{jk} = \dbyd{p^A_j}\dbyd{p^A_k} M^A.
\end{equation}
Note that $\bM^A_{jk}$ may be different from $\bM^A_{kj}$, since
differentiations with respect to different rotation axes do not commute.

Then the intersite distance $R$ can be written as
\begin{equation}
R^2 = \bR\cdot\bR = |\bB+\bb-\bA-\ba|^2 = |\bB+\bb-\bA|^2
        -2(\bB+\bb-\bA)\cdot\ba + \ba\cdot\ba.
\end{equation}
The first and last terms on the r.h.s. are constant with respect to
rotations of $A$, as is the factor $\bB+\bb-\bA$ in the middle term,
while $\ba=M^A \tilde\ba$ and $\tilde\ba$ is also constant.
Consequently 
\begin{equation}
\dbyd{p^A_k}(R^2) = -2\dbyd{p^A_k}(\bB+\bb-\bA)\cdot\ba
     = -2 (\bB+\bb-\bA)\cdot\bigl(\bM^A_k \tilde\ba\bigr),
\end{equation}
and
\begin{equation}
\dbyd[R]{p^A_k} = \tfrac{1}{2} (R^2)^{-1/2} (R^2)_k
     = -\frac{1}{R}(\bB+\bb-\bA)\cdot(\bM^A_k \tilde \ba),
\end{equation}
or in suffix notation,
\begin{equation}
\dbyd[R]{p^A_k} = -\frac{1}{R}(\bB+\bb-\bA)_\alpha (\bM^A_k)_{\alpha\beta}
      \tilde a_\beta.
\label{eq:dR}
\end{equation}

Next we need derivatives of the set of direction cosines $\Rhat\cdot\bW_a$
between the intersite unit vector $\Rhat$ and the local axes $\bW_a$
for site $a$. Using suffix notation, and working initially
with the vector $\bR$ rather than the unit vector $\Rhat$, we have
\begin{align}
R_\alpha\bW^a_{\alpha\beta} &= (B_\alpha+b_\alpha-A_\alpha-a_\alpha)
    \bM^A_{\alpha\gamma}\widetilde{\bW}^a_{\gamma\beta}\notag\\
  &= (B_\alpha+b_\alpha-A_\alpha) 
             \bM^A_{\alpha\gamma}\widetilde{\bW}^a_{\gamma\beta}
    -\bM^A_{\alpha\delta}\tilde a_\delta
             \bM^A_{\alpha\gamma}\widetilde{\bW}^a_{\gamma\beta}.
\end{align}
In the last term, $(\bM^A)_{\alpha\delta}(\bM^A)_{\alpha\gamma} =
\delta_{\delta\gamma}$, so this term becomes 
$\tilde a_\gamma \widetilde{\bW}^a_{\gamma\beta}$ and is independent of
the molecular orientation. Consequently
\begin{equation}
\dbyd{p^A_k} \bigl(R_\alpha\bW^a_{\alpha\beta}\bigr) = (B_\alpha+b_\alpha-A_\alpha) 
             (\bM^A_k)_{\alpha\gamma}\widetilde{\bW}^a_{\gamma\beta}.
\label{eq:dRWa.pA}
\end{equation}
The derivative of the scalar product $\bR\cdot\bw_b$ is different. Here we have
\begin{align}
-R_\alpha\bW^b_{\alpha\beta} &=
-(B_\alpha+b_\alpha-A_\alpha-\bM^A_{\alpha\gamma}\tilde a_\gamma)
    {\bW}^b_{\alpha\beta}\notag\\
  &= -(B_\alpha+b_\alpha-A_\alpha) {\bW}^b_{\alpha\beta}
    +\bM^A_{\alpha\gamma}\tilde a_\gamma {\bW}^b_{\alpha\beta}.
\end{align}
This time the first term is constant with respect
to rotation of $A$ and the second term provides the
derivative:
\begin{equation}
-\dbyd{p^A_k} \bigl(R_\alpha\bW^b_{\alpha\beta}\bigr) = 
+ (\bM^A_k)_{\alpha\gamma}\tilde a_\gamma {\bW}^b_{\alpha\beta}.
\end{equation}
Similarly, for the derivatives with respect to rotation of $B$, we have
\begin{align}
\dbyd{p^B_k}\bigl(R_\alpha\bW^a_{\alpha\beta}\bigr)
    &= \dbyd{p^B_k}\bigl((B_\alpha+M^B_{\alpha\gamma}\tilde b_\gamma-A_\alpha-a_\alpha)
             \bW^a_{\alpha\beta}\bigr)\notag\\
    &= (M^B_k)_{\alpha\gamma}\tilde b_\gamma \bW^a_{\alpha\beta},
\end{align}
and
\begin{align}
\dbyd{p^B_k}\bigl(-R_\alpha\bW^b_{\alpha\beta}\bigr)
    &= -\dbyd{p^B_k}\bigl((B_\alpha-A_\alpha-a_\alpha)
             M^B_{\alpha\gamma}\widetilde\bW^b_{\gamma\beta}
 + b_\alpha\bW^b_{\alpha\beta}\bigr) \notag\\
    &= -(B_\alpha-A_\alpha-a_\alpha)(M^B_k)_{\alpha\gamma}
             \widetilde\bW^b_{\gamma\beta}.
\label{eq:RWb.pB}
\end{align}

Eqs.~\eqref{eq:dRWa.pA}--\eqref{eq:RWb.pB} provide derivatives of the
scalar products 
$R_\alpha\bW^a_{\alpha\beta}$ and $-R_\alpha\bW^b_{\alpha\beta}$, but
we need the scalar products with the unit vector $\Rhat$. For example,
\begin{align}
\dbyd{p^A_k}  \bigl(\hat R_\alpha\bW^a_{\alpha\beta} \bigr)
    &= \dbyd{p^A_k} \biggl(\frac{1}{R}R_\alpha\bW^a_{\alpha\beta}\biggr)\notag\\
    &= \frac{1}{R}\dbyd{p^A_k} \bigl( R_\alpha\bW^a_{\alpha\beta}\bigr)
    - \frac{R_\alpha}{R^2} \bW^a_{\alpha\beta} \dbyd[R]{p^A_k}
\end{align}
The derivative in the first term is given by eq.~\eqref{eq:dRWa.pA}, and
the derivative in the second term by eq.~\eqref{eq:dR}. Similarly, for
the derivative with respect to rotation of $B$:
\begin{align}
\dbyd{p^B_k}  \bigl(\hat R_\alpha\bW^a_{\alpha\beta} \bigr)
  = \frac{1}{R}(M^B_k)_{\alpha\gamma}\tilde b_\gamma \bW^a_{\alpha\beta}
     - \frac{R_\alpha}{R^2}\bW^a_{\alpha\beta} \dbyd[R]{p^B_k}.
\end{align}

Finally we need the derivatives of the direction cosines between the
local axes of site $a$ and those of site $b$. Again writing the local
axis matrix for site $a$ as $\bW^a=\bM^A\widetilde{\bW}^a$, these direction
cosines are the elements of the matrix
\begin{equation}
(\bW^a)^T\bW^b = \bigl(\bM^A\widetilde{\bW}^a\bigr)^T\bM^B \widetilde{\bW}^b.
\end{equation}
(where $T$ denotes transpose) and the derivatives with respect to
rotations of $A$ are then just 
\begin{equation}
\dbyd{p^A_k}(\bW^a)^T\bW^b = \bigl(\bM^A_k\widetilde{\bW}^a\bigr)^T\bM^B \widetilde{\bW}^b.
\end{equation}

\subsection{Derivatives of the orientation matrices}

\subsubsection{Angle-axis coordinates}

This derivation follows \cite{ChakrabartiW09}. Rotations are 
described by a vector $\bp$, whose direction is the axis 
of rotation and whose magnitude is the rotation angle $\psi$ (in
radians). We define $\bn=\bp/\psi$, the unit vector in the
direction of the rotation axis. Associated 
with the vector $\bn$ is the matrix
\begin{equation}
\bN =
\begin{pmatrix}
0 & -n_3 & n_2 \\
n_3 & 0 & -n_1 \\
-n_2 & n_1 & 0
\end{pmatrix},
\end{equation}
so that $N_{ij} = -\epsilon_{ijk} n_k$. The rotation matrix
corresponding to the rotation $\bp$ is
\begin{equation}
\bM = \bI + \bN^2 (1-\cos\psi) + \bN \sin\psi,
\end{equation}
where $\bI$ is the identity matrix \cite[p.~75]{Altmann86}.

Now $\psi=\sqrt{p_1^2+p_2^2+p_3^2}$, so $\partial\psi/\partial
p_k=p_k/\psi=n_k$, and the derivative of the rotation matrix $\bM$
with respect to the component $p_k$ of $\bp$ is
\begin{equation}
\bM_k = \bN^2n_k\sin\psi + (\bN_k\bN+\bN\bN_k)(1-\cos\psi)
    + \bN n_k\cos\psi + \bN_k \sin\psi,
\label{eq:Mk}
\end{equation}
where $\bN_k$ is the derivative of $\bN$ with respect to $p_k$:
\begin{align}
\bN_k = \dbyd{p_k} \bN = \dbyd{p_k} \frac{1}{\psi}
\begin{pmatrix}
0 & -p_3 & p_2 \\
p_3 & 0 & -p_1 \\
-p_2 & p_1 & 0
\end{pmatrix}
= -\frac{n_k}{\psi}\bN + \frac{1}{\psi}
\begin{pmatrix}
0 & -\delta_{3k} & \delta_{2k} \\
\delta_{3k} & 0 & -\delta_{1k} \\
-\delta_{2k} & \delta_{1k} & 0
\end{pmatrix}.
\end{align}
When the rotation angle $\psi$ is zero, $\bn$ and the matrix
$\bN$ become undefined, but for small $\psi$,
\begin{equation}
  \bM = \bI + \psi\bN + \half\psi^2\bN^2 + O(\psi^3),
\end{equation}
so the limit of $\bM_k$ as $\psi\to 0$ is well defined:
\begin{equation}
\lim_{\psi\to 0} \bM_k = 
\begin{pmatrix}
0 & -\delta_{3k} & \delta_{2k} \\
\delta_{3k} & 0 & -\delta_{1k} \\
-\delta_{2k} & \delta_{1k} & 0
\end{pmatrix}.
\end{equation}

\section{Localization procedures}

\subsection{LeSueur--Stone procedure}

This method was described in \citet{LeSueurS94}. It deals with
non-local polarizabilities $\alpha^{ab}_{tu}$ by describing the
induced multipoles on $b$ using multipole expansions about $a$ and
\emph{vice versa}, irrespective of the distance between sites $a$ and
$b$. This was a workable procedure for small molecules but less
satisfactory for larger ones, where it was found necessary to retain
the non-local charge-flow polarizabilities to get satisfactory
results. Consequently it is no longer recommended, though it is still
available in the Orient program.

\subsection{Lillestolen--Wheatley procedure}

This is now the recommended localization procedure. The implementation
follows closely the account in \citet{LillestolenW07}. It is most easily
understood for the elimination of the charge-flow terms, which is in
any case the first step.

Consider the application of a uniform electric field $E_x$ in the $x$
direction. This will cause charge to flow between sites according to
the elements of the polarizability matrices:
\begin{equation}
\delta Q_0^a = - \sum_b \alpha^{ab}_{0x} V^b_x = \sum_b \alpha^{ab}_{0x} E_x, 
\end{equation}
since $V^b_x = \partial V^b/\partial x = -E_x$. The localization
procedure seeks to assign these non-local induced charges to neighbouring atoms,
with the element $\delta Q_0^a$ that is to be assigned to
site $c$ being represented by a multipole expansion centred on site
$c$. In the language of the Orient program, this amount of charge
is `shifted' from site $a$ to site $c$.

The question is how to divide up the induced charge $q_a = \delta Q_0^a$ among the
other sites. We want to move charge between neighbouring
(directly-bonded) sites as much as possible, so, following
\citet{LillestolenW07}, we require that charge
is to flow between polarizable sites at a rate that is
proportional to the difference in induced charges between sites, with
a rate constant $k_{ab}$ that is 1 if the sites $a$ and $b$ are
directly bonded and zero otherwise: 
\begin{equation}
\dbyd{t}q_a = \sum_{b\neq a} k_{ab}(q_b(t) - q_a(t))
\label{eq:rate}
\end{equation}
with $q_a(0)$ the initial induced charge on site $a$.
This can be written
\begin{equation}
  \dbyd{t}\bq = B \bq(t),
  \label{eq:rate2}
  \end{equation}
where $\bq(t)$ is the vector of induced charges and $B$ is the `bond
matrix', with elements
\begin{equation}
  B_{ab} = \begin{cases}
    +1 & \text{if $a$ and $b$ are directly bonded},\\
      -N_a & \text{if $a=b$},\\
      0 & \text{otherwise},
      \end{cases}
  \end{equation}
where $N_a$ is the number of polarizable sites directly bonded to $a$.
(Note that $q_a$ occurs $N_a$ times with non-zero $k_{ab}$ in
the sum over $b$ in eq.~\eqref{eq:rate}.)

It is convenient to transform eq.~\eqref{eq:rate2} so as to
diagonalize $B = S\Lambda S^T$, which gives a set of equations
\begin{equation}
\dbyd{t}\bq = S\Lambda S^T\bq(t),
  \end{equation}
or
\begin{equation}
\dbyd{t}q_a = \sum_{cb} S_{ac}\lambda_c S_{cd} q_d(t).
\label{eq:rate3}
\end{equation}
Now one of the eigenvalues $\lambda_c$ is zero; it arises from summing
eq.~\eqref{eq:rate} over $a$, and describes the fact that the sum of
all the charges is constant. We can drop this term from the sum over $c$.
Now the solution of eq~\eqref{eq:rate3} is
\begin{equation}
q_a(t) = \sumprime_c \sum_b S_{ac}\exp(\lambda_c t) S_{cb} q_b(0),
\end{equation}
where the prime on the sum denotes the omission of the zero eigenvalue
and its eigenvector from the sum. The other eigenvalues are all
negative, so the $q_a$ all decay to zero.

The total charge flowing between $a$ and $b$ is
\begin{align}
  q(a\to b) &=\int_0^\infty k_{ab}(q_a(t) - q_b(t)) \d t\notag\\
  &= \int_0^\infty k_{ab}(\sumprime_c \sum_b (S_{ac}-S_{bc})\exp(\lambda_c t) S_{cb} q_b(0)\notag\\
  &= -\sumprime_c \sum_d k_{ab} (S_{ac}-S_{bc})(\lambda_c)^{-1} S_{cd} q_d(0)\notag\\
  &= (Z_{ad}-Z_{bd}) q_d(0).
\end{align}
This is handled by `shifting' $\half q(a\to b)$ from $a$ to $b$ (i.e. by
expressing that amount of charge on $a$ as a multipole expansion on
$b$), and `shifting' $-\half q(a\to b)$ from $b$ to $a$. This moves as
little charge as possible and also makes the procedure as symmetrical
as possible.

This model has been set out in terms of actual charges. In practice,
it is the polarizabilities that need to be modified. Thus we have a
set of nonlocal polarizabilities $\alpha^{ab}_{0x}$ which are to be
removed by expressing them as local polarizabilities
$\alpha^{aa}_{0x}$ and $\alpha^{bb}_{0x}$, together with the higher
polarizabilities that correspond to the higher terms in the multipole
expansion of the moved charges. There is also the corresponding set
$\alpha^{ba}_{x0}$ to be treated in the same way; that is, modifying
columns of the polarizability matrix instead of rows.

The method applies to any nonlocal polarizability. In fact the
procedure starts with the pure charge-flow polarizabilities
$\alpha^{ab}_{00}$. Because of the sum rules for charge-flow
polarizabilities, this removes all $\alpha^{aa}_{00}$ terms as well.
Then the non-local charge-dipole polarizabilities $\alpha^{ab}_{0t}$
and $\alpha^{ab}_{t0}$, $t = 10, 11c, 11s$ are removed, then the
charge--quadrupole terms, and so on. The dipole--dipole,
dipole--quadrupole, and higher non-local terms are treated in the same
way.

In the implementation in Orient, it is possible to specify an upper
limit to the rank of polarizability that is to be localized. In
particular, it is possible just to remove charge-flow terms and leave
higher-rank non-local terms.

If required, the \verb:VERBOSE: option can be specified in the
\verb:POLARIZABILITY: section of the Orient input to obtain details of
the steps in the localization procedure. This produces a lot of output.

\bibliography{macros,all}

\end{document}
