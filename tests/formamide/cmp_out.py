#!/usr/bin/python
#  -*-  coding:  utf-8  -*-

"""Compare output file with reference in check directory.
"""

from functions import *
import argparse
import re
# import os.path
# import string
# import subprocess

this = __file__
parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Compare output file with reference in check directory.
""",epilog="""
Usage: {} [out]
""".format(this))

parser.add_argument("file", help="File to compare with reference (default out)",
                    default=["out"], nargs="*")

args = parser.parse_args()

file = args.file[0]

chk = ofile("check/"+file)
new = ofile(file)
print "\nChecking {} against {}".format(file,"check/"+file)

if cmp_after("Target grid",chk,new,skip=1):
  pass
else:
  print "File comparison failed"
  exit(1)
print "File comparison successful"
exit(0)
