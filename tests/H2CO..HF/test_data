Note Formaldehyde [5s4p3d/4s2p] with Hydrogen Fluoride [6s5p3d/4s3p]
 
Default rank 4 ( Multipoles up to hexadecapole unless otherwise specified )

Note  Stuff in parentheses is comment

Note  See *** below for explanation if the first test didn't work.

Note  Defining variables allows us to rerun the calculation using different
Note  values, without having to hunt through the dataset to find where they
Note  occur.
Variables
  theta 120 D
  x -2 A
  y  0 A
  z  4 A
End
 
Types
C   Z 6
O   Z 8
F   Z 9
H   Z 1
End

Note Hydrogen Fluoride [6s5p3d/4s3p]
 
Molecule 'Hydrogen Fluoride' at x y z rotated by theta about  0 1 0  linear
 
F   0.0 type F
       -0.43027667
       -0.08523512
        0.27619314
        0.01112383
        0.26343253
H   1.733069 type H bond to F
       0.43027667
       0.09655183
      -0.01467855
      -0.00338581
       0.03416361
 
End ( of HF )

Molecule Formaldehyde at 0.0 0.0 0.0 rotated by 0. about 0 0 1
Note  The optimization procedure requires that the molecule have a rotation
Note  axis and angle specified, even if the angle is zero.
 
C    0.0  0.0  0.0  type C
  0.655041
  0.354816  0.0  0.0
  0.025542  0.0  0.0  -0.385910  0.0
 -0.595851  0.0  0.0   2.346798  0.0  0.0  0.0
  2.172953  0.0  0.0  -0.568807  0.0  0.0  0.0  -0.149474  0.0
 
O    0.0  0.0  2.286570  type O bond to C
 -0.581386
  0.020940  0.0  0.0
  0.326349  0.0  0.0  -0.276484  0.0
 -0.200280  0.0  0.0  -0.829297  0.0  0.0  0.0
  0.224333  0.0  0.0   1.196370  0.0  0.0  0.0  0.016740  0.0
 
H1  -1.8133917  0.0  -1.0238796 type H bond to C
 -0.036827
 -0.121165   -0.199957  0.0
  0.003482   -0.089343  0.0  -0.026890  0.0
  0.041319   -0.050255  0.0  -0.049076  0.0   0.014862  0.0
  0.052035    0.011789  0.0  -0.058733  0.0  -0.030577  0.0   0.018400  0.0
 
H2   1.8133917  0.0  -1.0238796 type H bond to C
 -0.036827
 -0.121165    0.199957  0.0
  0.003482    0.089343  0.0  -0.026890  0.0
  0.041319    0.050255  0.0  -0.049076  0.0  -0.014862  0.0
  0.052035   -0.011789  0.0  -0.058733  0.0   0.030577  0.0   0.018400  0.0
 
End ( of Formaldehyde )
 
!  Exp-6 parameters from Mirsky, with harmonic mean combining rule for
!  alpha, arithmetic mean for rho, and geometric mean for C6.
Units bohr Hartree
Pairs
   Damping off
   H  H        alpha rho  C6
     00 00 0  2.270 3.95  2.10
     end
   H  C        alpha rho  C6
     00 00 0  2.085 4.94  8.56
     end
   H  O        alpha rho  C6
     00 00 0  2.238 4.62  6.39
     end
   C  C        alpha rho C6
     00 00 0  1.947 5.98 30.55
     end
   O  O        alpha rho C6
     00 00 0  2.212 5.30 18.83
     end
   F  F        alpha rho C6
     00 00 0  2.196 5.06 10.74
     end
   F  H        alpha rho  C6
     00 00 0  2.232 4.51  4.75
     end
   F  C        alpha rho  C6
     00 00 0  2.078 5.52 18.11
     end
   F  O        alpha rho  C6
     00 00 0  2.222 5.18 14.22
     end
End

Title 'Formaldehyde ... Hydrogen Fluoride'
 
Note  Print results in Angstrom and kJ/mol
Units Angstrom kJ/mol

Note  Calculate the energy of the starting configuration, giving details of
Note  the energy contributions
Energy details

Finish

Options
  ! Show steps gradient variables hessian
End

Switch induce on
Note  Optimize the geometry
Minimize
  frequency file freq
  plot xyz new file min.xyz title "Minimum"
end

Note  Print out the moments of inertia, rotational constants and overall
Note  multipole moments of the system
Show system inertia multipoles

Note  Find a saddle point

Note  *** It may be necessary to change "mode 1" to "mode -1" or vice
Note  *** versa to get output that agrees with the test file. The
Note  *** search starts in an arbitrary direction, but changing the
Note  *** sign reverses that direction.

Saddle
  dump dump.ts
  mode -1
  plot xyz new file ts.xyz title "Saddle search"
end

Finish

Note  Follow paths in each direction from the saddle point down to a minimum.
Note  We restore the geometry from the dump file in both cases (even though
Note  we're already there the first time) because small rounding errors can
Note  change the signs of the Hessian eigenvectors.
Optimize
  restart dump.ts
  minimum
  mode 1
  plot xyz new file path+.xyz every 1 title "Pathway 1"
end

Optimize
  restart dump.ts
  minimum
  mode -1
  plot xyz new file path-.xyz every 1 title "Pathway -1"
end

Finish

