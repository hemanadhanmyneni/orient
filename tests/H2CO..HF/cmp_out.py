#!/usr/bin/python
#  -*-  coding:  utf-8  -*-

"""Compare output file with reference in check directory.
"""

from functions import *
import argparse
import re
# import os.path
# import string
# import subprocess

this = __file__
parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Compare output file with reference in check directory.
""",epilog="""
Usage: {} out
""".format(this))

parser.add_argument("file")

args = parser.parse_args()

eps = 1.0e-6

chk = ofile("check/"+args.file)
new = ofile(args.file)
print "\nChecking {} against {}".format(args.file,"check/"+args.file)

go_to("Minimum",chk,new)
go_to("Total energy",chk,new)
Ea = get_float(chk.lines[chk.p])
Eb = get_float(new.lines[new.p])
if abs(Ea-Eb) < eps:
  print "Minimum energies match"
else:
  die("Minimum energy mismatch")

go_to("Saddle search",chk,new)
go_to("Total energy",chk,new)
Ea = get_float(chk.lines[chk.p])
Eb = get_float(new.lines[new.p])
if abs(Ea-Eb) < eps:
  print "Saddle-point energies match"
else:
  die("Saddle-point energy mismatch")

print "File comparison successful"
exit(0)
