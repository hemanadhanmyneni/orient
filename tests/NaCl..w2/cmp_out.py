#!/usr/bin/python
#  -*-  coding:  utf-8  -*-

"""Compare output file with reference in check directory.
"""

from functions import *
import argparse
import re
# import os.path
# import string
# import subprocess

this = __file__
parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Compare output file with reference in check directory.
""",epilog="""
Usage: {} out
""".format(this))

parser.add_argument("file", default=["out"], nargs="*")

args = parser.parse_args()

eps = 1.0e-6
ok = True

file = args.file[0]

chk = ofile("check/"+file)
new = ofile(file)
print "\nChecking {} against {}".format(file,"check/"+file)

go_to("Secondary minimum",chk,new)
go_to("Total energy",chk,new)
Ea = get_float(chk.lines[chk.p])
Eb = get_float(new.lines[new.p])
if abs(Ea-Eb) < eps:
  print "Secondary minimum: energies match"
else:
  print "Secondary minimum: energy mismatch"
  ok = False

go_to("Global minimum",chk,new)
go_to("Total energy",chk,new)
Ea = get_float(chk.lines[chk.p])
Eb = get_float(new.lines[new.p])
if abs(Ea-Eb) < eps:
  print "Global minimum: energies match"
else:
  print "Global minimum: energy mismatch"
  ok = False

if ok:
  print "File comparison successful"
  exit(0)
else:
  print "File comparison failed"
  exit(1)
  
