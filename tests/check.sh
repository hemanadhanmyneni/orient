#! /bin/csh -f
while ( $#argv )
  switch ($1)
  case -l:
    shift
    set log = $1
    breaksw
  default:
    echo "Flag $1 not recognised"
    exit 1
  endsw
  shift
end

foreach file ( check/* )
  set file = $file:t
  diff -w check/$file $file > $file.diff
  if ( $status > 0 ) then
    echo "$file differs from check file" | tee -a $log
    set diffs
  else
    rm $file.diff
  endif
end
if ( $?diffs ) then
  echo "Output files differ" | tee -a $log
else
  echo "Test O.K." | tee -a $log
endif
