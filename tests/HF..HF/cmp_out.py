#!/usr/bin/python
#  -*-  coding:  utf-8  -*-

"""Compare output file with reference in check directory.
"""

from functions import *
import argparse
import re
# import os.path
# import string
# import subprocess

this = __file__
parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Compare output file with reference in check directory.
""",epilog="""
Usage: {} out
""".format(this))

parser.add_argument("file")

args = parser.parse_args()

eps = 1.0e-6

chk = ofile("check/"+args.file)
new = ofile(args.file)
print "\nChecking {} against {}".format(args.file,"check/"+args.file)

go_to("Minimum",chk,new)
ok = cmp_after("Total moments for system, referred to inertial axes and centre of mass",chk,new)

go_to("Saddle point",chk,new)
ok = ok and cmp_after("Total moments for system, referred to inertial axes and centre of mass",chk,new)


if ok:
  print "File comparison successful"
  exit(0)
else:
  print "File comparison failed"
  exit(1)
