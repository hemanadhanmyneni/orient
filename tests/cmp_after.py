#!/usr/bin/python
#  -*-  coding:  utf-8  -*-

"""Compare output file with reference in check directory.
"""

from functions import *
import argparse
import re
# import os.path
# import string
# import subprocess

this = __file__
parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Compare output file with reference in check directory.
""",epilog="""
Usage: {} out
""".format(this))

parser.add_argument("file")
parser.add_argument("patterns", nargs="+")

args = parser.parse_args()

eps = 1.0e-6
ok = True

file = args.file

chk = ofile("check/"+file)
new = ofile(file)
print "\nChecking {} against {}".format(file,"check/"+file)

show = False
for pattern in args.patterns:
  if pattern == "--show":
    show = True
  elif pattern == "--quiet":
    show = False
  else:
    ok = ok and cmp_after(pattern,chk,new,show=show)

if ok:
  print "File out checked: OK"
  exit(0)
else:
  exit(1)
