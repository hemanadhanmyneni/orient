#!/usr/bin/python
#  -*-  coding:  utf-8  -*-

"""Check output file for energy minima.
"""

from functions import ofile
import argparse
import re
# import os.path
# import string
# import subprocess

this = __file__
parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Check output file for energy minima.
""",epilog="""
Usage: {} out
""".format(this))

parser.add_argument("file", default=["out"], nargs="*")

args = parser.parse_args()

eps = 1.0e-6
ok = True
quenches = 0

file = args.file[0]

# chk = ofile("check/"+file)
new = ofile(file)
print "\nChecking {}".format(file,"check/"+file)
minima = {
  "-116.590619": 0,
  "-95.029567": 0,
  "-93.063992": 0,
  "-93.677782": 0,
  "-85.452734": 0,
  "-100.505135": 0,
  "-84.301791": 0,
  }

end = False     
ok = True
new.p = 0
while True:
  if new.p >= len(new.lines):
    break
  m= re.match("Step \d+: +new minimum \d+, energy = +(-?\d+\.\d+)", new.lines[new.p])
  if m:
    F = m.group(1)
    old = False
    for E in minima.keys():
      if abs(float(F)-float(E)) < 1e-4:
        minima[E] += 1
        old = True
    if not old:
      print "New minimum {}".format(F)
      ok = False
  new.p += 1

print "Minima found:"
for E in sorted(minima.keys()):
  print "{:>12s} {:2d}".format(E, minima[E])
  
if ok:
  print "Test output checked."
  exit(0)
else:
  print """Unexpected new minimum in file {}.
These may be valid local minima -- check frequencies.""".format(file);
  exit (1)


