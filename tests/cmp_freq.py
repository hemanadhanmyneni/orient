#!/usr/bin/python
#  -*-  coding:  utf-8  -*-

"""Compare Orient frequency file with reference.
"""

from functions import *
import argparse
import re
from math import sqrt
# import os.path
# import string
# import subprocess
import sys

this = __file__
parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Compare orient frequency file with reference.
""",epilog="""
Usage: {} file1 [file2]
Compare frequency files.
If one file argument is given, file2 is taken to be check/file1.
""".format(this))

parser.add_argument("file", help="Files to compare", nargs="+")

args = parser.parse_args()

file1 = args.file[0]
f1 = ofile(file1)
if len(args.file) > 1:
  file2 = args.file[1]
else:
  file2 = "check/"+file1
f2 = ofile(file2)
print "\nChecking {} against {}".format(file1,file2)

eps = 1.0e-5
p = 0
ok = True
while p < len(f1.lines) and p < len(f2.lines):
  m1 = re.match(r' +(-?\d+\.\d+)([i ])?$', f1.lines[p])
  m2 = re.match(r' +(-?\d+\.\d+)([i ])?$', f2.lines[p])
  if m1 and m2:
    x1 = float(m1.group(1)); i1 = m1.group(2)
    x2 = float(m2.group(1)); i2 = m2.group(2)
    if abs(x1-x2) > eps and i1 == i2 or sqrt(x1**2+x2**2) > eps and i1 != i2:
      print "Mismatch at line {:1d}:\n{}{}".format(p,f1.lines[p],f2.lines[p])
      ok = False
  else:
    if not m1:
      print "Unexpected line {:1d} in file {}\n{}".format(p,file1,f1.lines[p])
    if not m2:
      print "Unexpected line {:1d} in file {}\n{}".format(p,file2,f2.lines[p])
    exit(2)
  p += 1

if ok:
  print "Files {} and {} match to within {:6.1e}".format(file1,file2,eps)
  sys.exit(0)
else:
  sys.exit(1)
