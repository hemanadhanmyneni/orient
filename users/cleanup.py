#!/usr/bin/python
#  -*-  coding:  utf-8  -*-

"""Clean up the list of email addresses exported from phpMyAdmin.
"""

import argparse
import re
# import os.path
# import string
# import subprocess

this = __file__
parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Clean up the list of email addresses exported from phpMyAdmin.
""",epilog="""
Convert the CSV list exported from the user database into a list
of email addresses.

The csv file is generated from the table licences.user using phpAdmin with
the SQL bookmarked as "orient user emails", i.e.

SELECT concat( '"', forename, " ", surname, '" <', email, '>' ) AS email
FROM `user`
WHERE orient LIKE "Y"
ORDER BY surname;

and exported to CSV using default options.
Usage: {} csv-file --out email-file

""".format(this))


parser.add_argument("csv", help="CSV file from phpMyAdmin")
parser.add_argument("--out", help="Output file")

args = parser.parse_args()

with open(args.csv) as CSV, open(args.out,"w") as OUT: 
  for line in CSV:
    OUT.write(re.sub(r'&quot;', '', line))


