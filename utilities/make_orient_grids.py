#!/usr/bin/python
#  -*-  coding  iso-8859-1  -*-

"""Make Orient grid using geometry from an xyz file.
"""

import argparse
import re
import os.path
# import string
import subprocess

parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Make Orient grid using geometry from an xyz file.
""",epilog="""
Construct Orient surface grid for the specified molecule, using the
molecular geometry from the specified xyz (or sites) file. The step
size and scale factor for the grid can be specified. A file containing
just the grid points and not the triangles is also generated, for
use in a CamCASP energy scan. The file names are generated from
the base name of the xyz file.
E.g.
./make_orient_grids.py diglycine psi4/digly_68_-80.xyz --vdw 1.5 --step 0.5
will produce a grid file digly_68_-80_vdW1.500.grid and a points
file digly_68_-80_vdW1.500.pts.

The default step size is 1.0 bohr, which is rather coarse. The default
vdW scale factor is 1.0, which is smaller than would normally be
required.

The prefix for the output file names is by default generated from the
basename of the xyz or sites file, but it can be specified if required.

E.g.
make_orient_grids.py digly --xyz diglycine.xyz --scale 1.50 --step 0.5 \\
  --prefix digly_180_180
""")


parser.add_argument("name", help="molecule name")
g = parser.add_mutually_exclusive_group(required=True)
g.add_argument("--sites", help="CamCASP sites file containing geometry")
g.add_argument("--xyz", help="File containing geometry in xyz format")
parser.add_argument("--vdw", help="vdW radius scaling factor",
                    type=float, default=1.0)
parser.add_argument("--step", type=float, default=1.0,
                    help="step size for underlying cubic grid (bohr)")
parser.add_argument("--prefix", help="Prefix for grid files")

args = parser.parse_args()

Z = {
"H": "1",
"C": "6",
"N": "7",
"O": "8"
}

if args.prefix:
  prefix = args.prefix
elif args.xyz:
  # print args.xyz
  prefix = re.sub(r'\.xyz', '', os.path.basename(args.xyz))
  # print prefix
elif args.sites:
  prefix = re.sub(r'\.sites', '', os.path.basename(args.sites))

type = {}
atoms = ""
if args.sites:
  title = args.name
  with open(args.sites) as S:
    natom = 0
    line = ""
    while not re.match(r'Sites', line, flags=re.I):
      line = S.readline()
    while not re.match(r'End', line, flags=re.I):
      m = re.match(r' *((\w)\w*)( +.*)$', line)
      if m:
        natom += 1
        atoms += "   {:4s}   {}  Type {}\n".format(
          m.group(1), m.group(3), m.group(1))
        type[m.group(1)] = Z[m.group(2)]
      line = S.readline()
elif args.xyz:
  with open(args.xyz) as XYZ:
    natom = int(XYZ.readline().strip())
    title = XYZ.readline()
    types = ""
    atoms = ""
    for count in range(natom):
      line = XYZ.readline()
      m = re.match(r' *((\w)\w*)( +.*)$', line)
      if m:
        atoms += "   {:4s}   {}  Type {}\n".format(
          m.group(1), m.group(3), m.group(1))
        type[m.group(1)] = Z[m.group(2)]


datafile = prefix+".in"
with open(datafile,"w") as IN:
  IN.write("""TITLE  {ti}

Parameters
      Sites          {n:2d}
      Types          {n:2d}
End

Types
""".format(ti=title,n=natom+4))
  for t in type.keys():
    IN.write("   {:4s}  Z {:2s}\n".format(t,type[t]))
  IN.write("""
End

Units angstrom
Molecule {mol}
{at}
end

  """.format(mol=args.name,at=atoms))

  IN.write("""
Units Bohr kJ/mol

Display

  Grid
    Name {p}
    Molecule {n}
    Title "{n} surface  radius vdw x {sc}"
    Step {st:6.3f}
    Radii add 0.00 scale {sc:6.3f} 
  End

  Export points file {p}_vdW{sc:5.3f}.pts
  Export grid {p}_vdW{sc:5.3f}.grid

End


Finish 


""".format(n=args.name, p=prefix, st=args.step, sc=args.vdw))

with open(datafile) as IN:
  subprocess.call(["orient", datafile], stdin=IN, stderr=subprocess.STDOUT)

os.remove(datafile)
