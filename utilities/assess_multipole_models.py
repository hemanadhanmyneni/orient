#!/usr/bin/python
#  -*-  coding  iso-8859-1  -*-

"""Assess multipole models against an energy scan.
"""

import argparse
import re
# import os.path
# import string
import subprocess

parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Assess multipole models against an energy scan.
""",epilog="""
assess_multipole_models diglycine --mdfn file1 [file2] --grid grid-file \\
    --map mapfile --summary file [--keep]
   
The multipoles from file2, if specified, otherwise file1, are used to
generate the electrostatic potential on a surface defined for the molecule
defined in file1, and evaluate the diferences from the file1 map.

The molecule definitions must be provided in local axes if two files are
specified. Use local_mdfn.py to transform from global axes if necessary.
A grid file defined for file1 must be provided, and a map file giving the
file1 electrostatic potential on the grid, obtained using CamCASP or
possibly Orient.

A summary of the differences is provided in the specified output file.
The orient input and output files are deleted unless the --keep argument
is specified.

""")

parser.add_argument("name", help="Molecule name")
parser.add_argument("--mdfn", help="Molecule definition files", nargs="+")
parser.add_argument("--grid", help="Grid file", required=True)
parser.add_argument("--map", help="Electrostatic potential map file",
                    required=True)
# parser.add_argument("--axes", help="File containing local axis definitions")
parser.add_argument("--summary", help="File for output summary",
                    default="summary")
parser.add_argument("--keep", help="Keep intermediate files",
                    action="store_true")
parser.add_argument("--nodisplay", help="Suppress display of difference maps",
                    action="store_true")

args = parser.parse_args()

def merge(file1,file2):
  #  Merge the multipoles from file 2 into the site positions from file1
  defn = ""
  cont1 = False
  cont2 = False
  line1 = "--"
  with open(file1) as S, open(file2) as M:
    while line1 != "" and line2 != "":
      line1 = S.readline()
      line2 = M.readline()
      if line1 == line2 or cont1:
        defn += line1
        if re.search(r'\+\+\+ *$', line1):
          cont1 = True
        else:
          cont1 = False
      elif re.match(r' *\w+ +at ', line1):
        defn += line1
        if re.search(r'\+\+\+ *$', line1):
          cont1 = True
      elif re.match(r' +-?\d+\.\d+', line2):
        defn += line2
    return(defn)

datafile = "orient.data"
with open(datafile,"w") as DATA:
  DATA.write("""!  Generic Orient data file for potential maps

Allocate
  Molecules 20
End

Types
  H  Z  1
  C  Z  6
  N  Z  7
  O  Z  8
  F  Z  9
  S  Z 16
  b  Z  0
  Q  Z  0
End

""")

  if len(args.mdfn) == 1:
    with open(args.mdfn[0]) as MDF:
      DATA.write(MDF.read())
  else:
    DATA.write(merge(args.mdfn[0],args.mdfn[1]))

  DATA.write("""
Edit {mol}
  Bonds auto
  ! verbose
End

Units Bohr eV
Display
  
  Allocate Maps 20

  Import grid {grid}
  Import map {map} elst-map

  map Lmax
    Molecule {mol}
    Title "{mol} full rank potential"
    potential
  End

End

Edit {mol}
  Limit rank 3 all
End

Display
  
  map L3
    Molecule {mol}
    Title "{mol} rank 3 potential"
    potential
  End

End

Edit {mol}
  Limit rank 2 all
End

Display
  
  map L2
    Molecule {mol}
    Title "{mol} rank 2 potential"
    potential
  End

End

Edit {mol}
  Limit rank 1 all
End

Display
  
  map L1
    Molecule {mol}
    Title "{mol} rank 1 potential"
    potential
  End

End

Edit {mol}
  Limit rank 0 all
End

Display
  
  map L0
    Molecule {mol}
    Title "{mol} rank 0 potential"
    potential
  End

  map Lmax-diff
    Difference Lmax minus elst-map
  end

  map L3-diff
    Difference L3 minus elst-map
  end

  map L2-diff
    Difference L2 minus elst-map
  end

  map L1-diff
    Difference L1 minus elst-map
  end

  map L0-diff
    Difference L0 minus elst-map
  end

  Describe elst-map
  Describe Lmax
  Describe L3
  Describe L2
  Describe L1
  Describe L0
  Describe Lmax-diff
  Describe L3-diff
  Describe L2-diff
  Describe L1-diff
  Describe L0-diff
  
""".format(mol=args.name,grid=args.grid,map=args.map))

  if not args.nodisplay:
    DATA.write("""
  Height 500 ( default is 500 )
  Viewpoint 1.0 0.0 0.0 up 0.0 0.0 1.0
  Viewport 12.0
  ticks -1.0 -0.5 0.0 0.5 1.0
  Colourscale min -1.5 max 1.5 unit eV
  ball-and-stick {mol}
  #include /home/ajs1/molecules/colourmaps/colourmap

  show elst-map
    Title "{mol} vdw1.5: E(1)elst"
  end

  show Lmax
    Title "{mol} vdw1.5: full rank"
  end

  show L0
    Title "{mol} vdw1.5: charges only"
  end

  show Lmax-diff
    Title "{mol} vdw1.5: full rank - elst-map"
  end

  show L3-diff
    Title "{mol} vdw1.5: rank 3 - elst-map"
  end

  show L2-diff
    Title "{mol} vdw1.5: rank 2 - elst-map"
  end

  show L1-diff
    Title "{mol} vdw1.5: rank 1 - elst-map"
  end

  show L0-diff
    Title "{mol} vdw1.5: charges only - elst-map"
  end

End
""".format(mol=args.name))

with open(args.summary,"w") as S:
  S.write("Map for {}\n".format(args.mdfn[0]))
  S.write("Multipoles from {}\n".format(args.mdfn[len(args.mdfn)-1]))

arglist = ["/home/ajs1/orient/trunk/bin/orient_display.py", datafile,
           "--summary", args.summary, "--show", "none"]
if args.nodisplay:
  pass
else:
  arglist += ["elst-map", "Lmax-diff", "L2-diff", "L1-diff", "L0-diff"]
if args.keep:
  arglist += ["--keep"]
subprocess.call(arglist)

