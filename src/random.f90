MODULE random

IMPLICIT NONE

PRIVATE
PUBLIC :: sdprnd, dprand

DOUBLE PRECISION, PARAMETER :: xmod = 1000009711.0d0,                  &
    ymod = 33554432.0d0,                                               &
    xmod2 = 2000019422.0d0,                                            &
    xmod4 = 4000038844.0d0,                                            &
    tiny = 1.0d-17
DOUBLE PRECISION :: poly(101), other, offset
INTEGER :: index
LOGICAL :: inital=.true.
SAVE inital

!   Copyright (C) 1992  N.M. Maclaren
!   Copyright (C) 1992  The University of Cambridge

!   This software may be reproduced and used freely, provided that all
!   users of it agree that the copyright holders are not liable for any
!   damage or injury caused by use of this software and that this
!   condition is passed onto all subsequent recipients of the software,
!   whether modified or not.

CONTAINS

SUBROUTINE sdprnd (seed)
IMPLICIT NONE

INTEGER, INTENT(IN) :: seed
INTEGER :: ix, iy, iz, i
DOUBLE PRECISION :: x

!   SEED should be set to an integer between 0 and 9999 inclusive;
!   a value of 0 will initialise the generator only if it has not
!   already been done.

if (inital .or. seed .ne. 0) then
  inital = .false.
else
  return
end if

!   INDEX must be initialised to an integer between 1 and 101
!   inclusive, POLY(1...N) to integers between 0 and 1000009710
!   inclusive (not all 0), and OTHER to a non-negative proper fraction
!   with denominator 33554432.  It uses the Wichmann-Hill generator to
!   do this.

ix = mod(abs(seed),10000)+1
iy = 2*ix+1
iz = 3*ix+1
do i = -10,101
  if (i .ge. 1) poly(i) = aint(xmod*x)
  ix = mod(171*ix,30269)
  iy = mod(172*iy,30307)
  iz = mod(170*iz,30323)
  x = mod(dble(ix)/30269.0d0+dble(iy)/30307.0d0+                &
      dble(iz)/30323.0d0,1.0d0)
end do
other = aint(ymod*x)/ymod
offset = 1.0d0/ymod
index = 1
END SUBROUTINE sdprnd

!-----------------------------------------------------------------------

DOUBLE PRECISION FUNCTION dprand()
IMPLICIT NONE
DOUBLE PRECISION :: x, y
INTEGER :: n

!   This returns a uniform (0,1) random number, with extremely good
!   uniformity properties.  It assumes that double precision provides
!   at least 33 bits of accuracy, and uses a power of two base.

if (inital) then
  call sdprnd (0)
  inital = .false.
end if

!   See [Knuth] for why this implements the algorithm described in
!   the paper.  Note that this code is tuned for machines with fast
!   double precision, but slow multiply and divide; many, many other
!   options are possible.

n = index-64
if (n .le. 0) n = n+101
x = poly(index)+poly(index)
x = xmod4-poly(n)-poly(n)-x-x-poly(index)
if (x .lt. 0.0d0) then
  if (x .lt. -xmod) x = x+xmod2
  if (x .lt. 0.0d0) x = x+xmod
else
  if (x .ge. xmod2) then
    x = x-xmod2
    if (x .ge. xmod) x = x-xmod
  end if
  if (x .ge. xmod) x = x-xmod
end if
poly(index) = x
index = index+1
if (index .gt. 101) index = index-101

!   Add in the second generator modulo 1, and force to be non-zero.
!   The restricted ranges largely cancel themselves out.

10 y = 37.0d0*other+offset
other = y-aint(y)
if (other .eq. 0.0d0) go to 10
x = x/xmod+other
if (x .ge. 1.0d0) x = x-1.0d0
dprand = x+tiny
END FUNCTION dprand

END MODULE random
