      MODULE indexes

!  Maintains a sparse matrix of values indexed by I,J. ROW(I) is a
!  pointer into row I of the matrix; if zero there are no non-zero
!  elements in that row. If non-zero it contains a pointer L to the
!  first of the non-zero elements. COL(L) is the column index J for
!  this element, ELEMENT(L) is its value, and NEXT(L) points to the next
!  element in the row, or is zero if there are no more. The elements
!  in any row are ordered in increasing value of the column number.
!  If the flag SYMM is nonzero, the matrix is assumed symmetric and the
!  only values stored in row I are those for columns J >= I.

!  The structure LL contains the data.

      IMPLICIT NONE
      TYPE index
          INTEGER :: free=0, limit, max
          LOGICAL symm
          INTEGER, POINTER :: row(:) => null()
          INTEGER, POINTER :: col(:) => null(), element(:) => null(),   &
              next(:) => null()
      END TYPE index

      PRIVATE
      PUBLIC :: lookup, insert, reset, entry, remove, index

      CONTAINS

      INTEGER FUNCTION lookup(LL,i,j)


!  LOOKUP finds the entry for row I, column J (or vice versa), and
!  returns zero if no such entry exists. It also returns zero if
!  either I or J is zero.

      IMPLICIT NONE
      TYPE(index) :: LL
      INTEGER :: i, j, l

      lookup=0
      if (i .le. 0 .or. j .le. 0) return
      if (i .le. j .or. .not. LL%symm) then
        l=LL%row(i)                  !  If zero, no entries in row i
        do while (l .gt. 0)       !  Search along row
          if (LL%col(l) .eq. j) then
            lookup=LL%element(l)
            return
          endif
          l=LL%next(l)
        end do
      else
        l=LL%row(j)
        do while (l .gt. 0)
          if (LL%col(l) .eq. i) then
            lookup=LL%element(l)
            return
          endif
          l=LL%next(l)
        end do
      endif
      END FUNCTION lookup

!--------------------------------------------------------------------

      SUBROUTINE insert (LL,i,j,n,name)

      USE input, only : die
      USE utility, only : stri
      IMPLICIT NONE
      CHARACTER(LEN=*), INTENT(IN) :: name
      TYPE(INDEX) :: LL
      INTEGER, INTENT(IN) :: i, j, n
      INTEGER :: k, l, m, ii, jj

      if (i .gt. j .and. LL%symm) then
        ii=j
        jj=i
      else
        ii=i
        jj=j
      endif
      if (ii .gt. LL%limit) then
        print "(2A)", "Unable to insert new entry in ", trim(name)
        call die ("Row index "//trim(stri(ii))//" too big",.false.)
      endif

      k=0
      l=LL%row(ii)

      do
        if (l .eq. 0) then
          ! Currently no entries in row ii, or none from col jj onwards
          exit
        else if (LL%col(l) .eq. jj) then
          LL%element(l)=n
          return
        else if (LL%col(l) .gt. 0 .and. LL%col(l) .lt. jj) then
          k=l
          l=LL%next(l)
          cycle
        else
          exit
        endif
      end do
      if (n .eq. 0) return

      !  New entry needed
      m=LL%free
      if (m .gt. LL%max) then
        print "(2A)", "Unable to insert new entry in ", trim(name)
        call die ("Not enough space",.false.)
      endif
      LL%free=LL%free+1
      LL%col(m)=jj
      LL%element(m)=n
      if (k .eq. 0) then
        !  Insert as first entry in row
        LL%next(m)=LL%row(ii)
        LL%row(ii)=m
      else
        !  Insert after entry k
        LL%next(m)=LL%next(k)
        LL%next(k)=m
      endif

      END SUBROUTINE insert

!--------------------------------------------------------------------

      SUBROUTINE remove(LL,i,j)

      ! USE input, only : die
      ! USE utility, only : stri
      IMPLICIT NONE
      TYPE(INDEX) :: LL
      INTEGER, INTENT(IN) :: i, j
      INTEGER :: k, l, ii, jj

      if (i .gt. j .and. LL%symm) then
        ii=j
        jj=i
      else
        ii=i
        jj=j
      endif

      k=0
      l=LL%row(ii)

      do
        if (l .eq. 0) then
          ! Currently no entries in row ii, or none from col jj onwards
          exit
        else if (LL%col(l) .eq. jj) then
          LL%element(l)=0
          if (k==0) then
            LL%row(ii)=LL%next(l)
          else
            LL%next(k)=LL%next(l)
          end if
          return
        else if (LL%col(l) .gt. 0 .and. LL%col(l) .lt. jj) then
          k=l
          l=LL%next(l)
          cycle
        else
          exit
        endif
      end do

      END SUBROUTINE remove

!--------------------------------------------------------------------

      SUBROUTINE reset(LL,newlimit,newmax,newsymm)

!  The routine reset allocates space for the data:

!    newlimit:   number of rows
!    newmax:     maximum number of non-zero elements
!    newsymm:    true if the matrix is symmetric.

      IMPLICIT NONE
      TYPE(INDEX) :: LL
      INTEGER :: newlimit, newmax
      LOGICAL :: newsymm

      LL%max=newmax
      LL%symm=newsymm
      if (associated(LL%row)) then
        if (ubound(LL%row,1) .lt. newlimit) then
          deallocate(LL%row)
        endif
      endif
      if (.not. associated(LL%row)) then
        allocate(LL%row(newlimit))
        LL%row(:)=0
        LL%limit=newlimit
      endif
      if (associated(LL%col)) then
        if (ubound(LL%col,1) .lt. newmax) then
          deallocate(LL%col,LL%element,LL%next)
        endif
      endif
      if (.not. associated(LL%col)) then
        allocate(LL%col(newmax), LL%element(newmax), LL%next(newmax))
        LL%max=newmax
      endif
      LL%free=1
      
      END SUBROUTINE reset

!------------------------------------------------------------------------

      SUBROUTINE entry(LL,i,j,m)

!  Returns the next entry in row I of the sparse matrix in LL.
!  If J = 0 on entry, the column number of a non-null entry is returned
!  in J and the value of that element in M. A subsequent entry with J
!  unchanged finds the next entry. If no entry is found, J is set to
!  zero.

      IMPLICIT NONE
      TYPE(INDEX) :: LL
      INTEGER :: i, j, l, m

      L=LL%row(i)
      do while (l .gt. 0)
        if (LL%col(l) .gt. j) then
          j=LL%col(l)
          m=LL%element(l)
          return
        else
          l=LL%next(l)
        endif
      end do
      j=0

      END SUBROUTINE entry

      END MODULE indexes
