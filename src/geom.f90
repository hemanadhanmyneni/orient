MODULE geom

PRIVATE
PUBLIC :: geometry

CONTAINS

SUBROUTINE geometry(mm)

!  Update the variable pointers sp for position and sr for orientation
!  from the position and orientation arrays sx and se.
!  Also update the mol(m)%cm and mol(m)%p vectors.

USE consts, ONLY : afact, rfact
USE input, only : die
USE variables
USE switches
USE sites
USE molecules, ONLY: mol, mols, head, cm
USE rotations, ONLY: quater, angleaxis

IMPLICIT NONE

INTEGER, INTENT(IN), OPTIONAL :: mm
INTEGER :: i, j, k, m, m1, m2
DOUBLE PRECISION :: quaternion(4), r, theta, phi, psi, sinpsi,   &
    alpha, beta, gamma

if (present(mm)) then
  m1=mm
  m2=mm
else
  m1=1
  m2=mols
endif

do m=m1,m2
  k=cm(m)
10 if (print .ge. 1) then
    write (6,'(/A)') name(k)
    write (6,'(A,3f12.5)') 'Position: ', (sx(i,k)*rfact, i=1,3)
  end if
  mol(m)%cm=sx(:,k)
  !  Reset position variables
  if (iand(sf(k),48) .eq. 32) then
    !  Cartesian
    do i=1,3
      call setval(sx(i,k),sp(i,k),1)
    end do
  else if (iand(sf(k),48) .eq. 16) then
    !  Polar
    r=sqrt(sx(1,k)**2+sx(2,k)**2+sx(3,k)**2)
    call setval(r,sp(1,k),1)
    if (r .gt. 1d-8) then
      theta=acos(sx(3,k)/r)
      call sec_atan2(sx(2,k),sx(1,k),phi)
      call setval(theta,sp(2,k),2)
      call setval(phi,sp(3,k),2)
    endif
  endif

  if (mol(m)%mdim .gt. 0) then
    if (print .ge. 1) then
      print '(A/(3F15.8))', 'Orientation matrix',                 &
          ((se(i,j,k), j=1,3), i=1,3)
    end if
    call angleaxis(se(:,:,k),mol(m)%p)
    !         cosb=max(-1d0,min(1d0,se(3,3,k)))
    !         call sec_acos(cosb,beta)
    beta=acos(max(-1d0,min(1d0,se(3,3,k))))
    if (beta .gt. 1d-6) then
      call sec_atan2(se(2,3,k),se(1,3,k),alpha)
      call sec_atan2(se(3,2,k),-se(3,1,k),gamma)
    else
      call sec_atan2(se(2,1,k),se(1,1,k),alpha)
      gamma=0d0
    endif
    if (print .ge. 1) then
      write (6,'(A,3F9.3)') 'Euler angles: ',                       &
          alpha*afact, beta*afact, gamma*afact
    end if
    !  Reset orientation variables
    if (iand(sf(k),3) .eq. 1) then
      !  Euler angles
      call setval(alpha,sr(1,k),2)
      call setval(beta,sr(2,k),2)
      call setval(gamma,sr(3,k),2)
    else if (iand(sf(k),3) .eq. 2) then
      !  Angle-axis
      call quater(se(:,:,k),quaternion)
      !           write (6,'(a,4f12.6)')
      !    &         'Quaternion parameters:', quaternion
      sinpsi=min(sqrt(quaternion(2)**2+                            &
          quaternion(3)**2+quaternion(4)**2),1d0)
      psi=2d0*asin(sinpsi)
      !           if (sinpsi .gt. 1d-8) then
      !           write (6,'(a,f8.3,a,3f10.5)')
      !    &          'Rotated by', psi*afact,
      !    &          ' about axis ', (quaternion(i)/sinpsi, i=2,4)
      !           endif
      call setval(psi,sr(1,k),2)
      if (psi .gt. 1d-6) then
        do i=2,4
          call setval(quaternion(i)/sinpsi,sr(i,k),0)
        end do
      endif
    else if (iand(sf(k),3) .ne. 0) then
      call die                                                  &
          ('Error in GEOMETRY: cannot reset orientation',.false.)
    endif
  endif
  if (k .ne. head(m)) then
    k=next(k)
    if (k .ne. 0) go to 10 ! Test included in case of programming error
  endif
end do

END SUBROUTINE geometry
!-----------------------------------------------------------------------
SUBROUTINE sec_atan2(a1,a2,result)

USE consts, ONLY : PI
IMPLICIT NONE

DOUBLE PRECISION a1,a2,result

if (a2 .eq. 0.0d0) then
  result=sign(0.5d0*PI,a1)
else
  result=atan2(a1,a2)
end if

END SUBROUTINE sec_atan2
!-----------------------------------------------------------------------
SUBROUTINE sec_acos(a,result)

IMPLICIT NONE

DOUBLE PRECISION a,result

result=acos(a)

END SUBROUTINE sec_acos

END MODULE geom
