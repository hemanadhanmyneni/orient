SUBROUTINE compare

!  Compare energies of a pair of molecules calculated from some model
!  with values provided from a CamCASP scan

USE input, ONLY: read_line, item, nitems, reada, readi, readf, upcase,  &
    char, find_io, reread, die
USE consts
USE force, ONLY: forces
USE molecules, ONLY: mols
USE variables, ONLY: value, findv

IMPLICIT NONE
INTEGER :: i, j, info, ix, m, n, u, npts=0, vlist(7), nv
DOUBLE PRECISION :: sum_diffs, sumsq_diffs, v
DOUBLE PRECISION :: es, er, eind, edisp, etotal, es_ref, er_ref,        &
    ind_ref, disp_ref
CHARACTER(LEN=20) :: key, ww
CHARACTER(LEN=80) :: scan_file=""
LOGICAL :: eof, elst=.false., exch=.false., ind=.false.,          &
    disp=.false., verbose=.false.

!  The COMPARE keyword has already been read
do while (item<nitems)
  call reada(key)
  select case(upcase(key))
  case("FILE")
    call reada(scan_file)
  case("VERBOSE")
    verbose=.true.
  case("QUIET")
    verbose=.false.
  case default
    call reread(-1)
    call reada(scan_file)
  end select
end do


if (scan_file == "") call die("No scan file specified",.true.)
u = find_io(21)
open(u,file=scan_file,status="old",iostat=info)
if (info > 0) call die("Can't open file "//trim(scan_file),.true.)

npts = 0
! data = .false.
preamble: do
  call read_line(eof,u)
  if (eof) exit
  if (verbose) print "(a)", trim(char)
  call reada(key)
  select case(upcase(key))
  case("", "!", "NOTE")
    cycle
  case("ENERGY-UNITS","LENGTH-UNITS")
    call reada(ww)
    call set_unit(ww)
  case("POINTS")
    call readi(npts)
  case("BEGIN")
    call reada(ww)
    if (upcase(ww) .ne. "DATA") then
        call die("Unknown preamble line BEGIN "//trim(ww),.true.)
    else
      ! data = .true.
      exit preamble
    end if
  case("NAMES")
    !  Read variables and data names
    nv = 0
    do while(item < nitems)
      call reada(ww)
      call findv(ww,ix)
      if (ix > 0) then
        nv = nv+1
        vlist(nv)=ix
      else
        select case(upcase(ww))
        case("E1ELST","E(1)ELST")
          elst = .true.
        case("E1EXCH","E(1)EXCH")
          exch = .true.
        case default
          call die("Can't handle energy term "//trim(ww)//" at present",.true.)
        end select
      end if
    end do
  end select
end do preamble

n=0
sum_diffs = 0d0
sumsq_diffs = 0d0
do
  call read_line(eof,u)
  if (eof) exit
  ! print "(a)", trim(char)
  call reada(ww)
  if (upcase(ww) == "END") exit
  call reread(-1)
  n = n+1
  do i=1,nv
    call readf(v)
    select case(i)
    case(1,2,3)
      v=v/rfact
    case(4)
      v=v/afact
    end select
    j=vlist(i)
    value(j)=v
  end do
  if (elst) call readf(es_ref,efact)
  if (exch) call readf(er_ref,efact)
  if (ind) call readf(ind_ref,efact)
  if (disp) call readf(disp_ref,efact)

  do m=1,mols
    call axes(m,.false.)
  end do

  call forces(0,50,es,er,eind,edisp,etotal)
  if (verbose) then
    print "(i5,3f14.5)", n, es_ref*efact, es*efact, (es_ref - es)*efact
  end if
  sum_diffs = sum_diffs + es_ref - es
  sumsq_diffs = sumsq_diffs + (es-es_ref)**2

end do

! print "(a,1p,e14.6,1x,a)", "Sum of differences = ", sum_diffs*efact,    &
!     trim(eunit)
! print "(a,1p,e14.6,1x,2a)", "Sumsq differences  = ",                    &
!     sumsq_diffs*(efact**2), trim(eunit), "^2"

print "(a,1p,e14.6,1x,a)", "Mean difference = ", (sum_diffs/n)*efact, eunit
print "(a,1p,e14.6,1x,a)", "s.d. difference = ",                        &
    sqrt((sumsq_diffs-(sum_diffs)**2/n)/(n-1))*efact, eunit

END SUBROUTINE compare
