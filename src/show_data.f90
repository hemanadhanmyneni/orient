MODULE show_data

USE consts
USE global, ONLY: iw
USE switches
USE variables
USE geom
USE input
USE parameters
USE indparams
USE properties
USE rotations, ONLY : wigner
USE sites
USE sizes
USE switches
USE types
USE molecules
USE induction
IMPLICIT NONE

PRIVATE
PUBLIC show, punchm

INTEGER :: liw

CONTAINS

SUBROUTINE show(file)

CHARACTER(LEN=*), INTENT(IN), OPTIONAL :: file

CHARACTER(LEN=20) :: ww, w
LOGICAL :: iaxes, reinput
INTEGER :: map(3), base, i, j, k, km, m
DOUBLE PRECISION :: qi(sq), qt(sq), v(sq,sq)
CHARACTER(LEN=3) :: xname(3)=(/'(x)', '(y)', '(z)'/)


if (present(file)) then
  liw = find_io(37)
  open(liw,file=file,status="replace",action="write")
else
  liw = iw
end if
iaxes=.false.
do while (item < nitems)
  call reada(ww)
  select case(upcase(ww))
  case("VARIABLES","CONSTANTS")
    call readu(w)
    select case(w)
    case("REINPUT")
      reinput=.true.
    case("")
      reinput=.false.
    case default
      call reread(-1)
    end select
    if (upcase(ww) == "VARIABLES") then
      call vprint('Variables:', 1,NVAR, reinput)
    else
      call vprint('Constants:',NVAL,NCONST, reinput)
    end if
  case("SWITCHES")
    print "(/A)", "Program switches:"
    print "(5(A,1X,L1,5x))", (swname(i), switch(i), i=1,10)
  case("TYPES")
    call show_types
  case('SYSTEM')
    do m=1,mols
      k=cm(m)
      do while (k .ne. 0)
        write (liw,'(i4,2x,a,3f15.8)') k, name(k), (sx(i,k), i=1,3)
        k=next(k)
      end do
      call axes(m,.false.)
      k=cm(m)
      do while (k .ne. 0)
        write (liw,'(i4,2x,a,3f15.8)') k, name(k), (sx(i,k), i=1,3)
        k=next(k)
      end do
    end do
    do while (item .lt. nitems)
      call readu(ww)
      select case(ww)
      case("INERTIA")
        call inertia(0)
        j=1
        if (mi(2,0) > mi(j,0)) j=2
        if (mi(3,0) > mi(j,0)) j=3
        map(3)=j
        map(1)=mod(j,3)+1
        map(2)=mod(j+1,3)+1
        if (mi(map(1),0) > mi(map(2),0)) then
          j=map(1)
          map(1)=map(2)
          map(2)=j
        endif
!  I_A = mi(map(1),0), I_B = mi(map(2),0), I_C = mi(map(3),0)
        m=3
        if (mi(map(1),0) .lt. 1d-8                     & ! Linear
            .or. mi(map(3),0)-mi(map(1),0) .lt. 1d-8)  & ! Spherical top
            m=1
        write (liw,'(/a,2a,9x,2a,9x,2a/t34,3f14.6)')              &
            'Moments of inertia (a.m.u. bohr^2):    ',            &
            'A ', xname(map(1)), 'B ', xname(map(2)),             &
            'C ', xname(map(3)), (mi(map(i),0), i=1,3)
        if (m .eq. 1) then
          write (liw,'(/a, t46, f14.8/a, t46, f14.8)')            &
              'Rotational constant (cm-1): ', MItoB/mi(map(2),0), &
              '                     (GHz): ',                     &
              (MItoB/mi(map(2),0))*EHVGHZ/EHVCMM
        else
          write (liw,'(/a, t34, 3f14.8/a, t34, 3f14.8)')          &
              'Rotational constants (cm-1): ',                    &
              (MItoB/mi(map(i),0), i=1,3),                        &
              '                     (GHz):  ',                    &
              ((MItoB/mi(map(i),0))*EHVGHZ/EHVCMM, i=1,3)
        endif
        iaxes=.true.
      case("MULTIPOLES","MOMENTS")
        call totalq(0,qt,.true.)
        write (liw,'(/a,a)') 'Total moments for system, ',        &
            'referred to global axes and origin'
        call printq(qt,4,                                         &
            '(t5, a, t18, 5f12.6:/1x,t18,4f12.6)')
        if (iaxes) then
          km=0
          qi(:)=0d0
          call shiftq(qt, 0,4,                                    &
              -sx(1,km),-sx(2,km),-sx(3,km), qi,4)
          call wigner(se(:,:,km),v,4)
          qt=matmul(qi,v)
          ! call dgemv('T',25,25,1d0,v,sq, qt,1,0d0,qi,1)
          write (liw,'(/a,a)') 'Total moments for system, ',      &
              'referred to inertial axes and centre of mass'
          call printq(qt,4,                                       &
              '(t5, a, t18, 5f12.6:/1x,t18,4f12.6)')
          write (liw,'(/t24,6a/t5,a,3f12.6)')                     &
              'a ', xname(map(1)), '       b ', xname(map(2)),    &
              '       c ', xname(map(3)), 'Dipole/Debye:',        &
              qt(mod(map(1),3)+2)*auVDebye,                       &
              qt(mod(map(2),3)+2)*auVDebye,                       &
              qt(mod(map(3),3)+2)*auVDebye
        endif
      end select
    end do
  case("ALL")
    call readu(ww)
    do m=1,mols
      call show_molecule(m,ww)
    end do
  case("MOLECULE")
    !  Molecule name should follow
    cycle
  case default
    !  Assume that we have a molecule name
    m=locate(ww)
    if (m == 0)                                                 &
        call die('Unrecognized keyword or molecule name '//     &
        trim(ww)//' in SHOW command',.true.)
    call readu(ww)
    call show_molecule(m,ww)
  end select

end do

CONTAINS

SUBROUTINE show_molecule(m,w)

IMPLICIT NONE
INTEGER m
CHARACTER*(20) :: w
! CHARACTER*(80) :: filename=""

calc(m)=.false.
select case(w)
case('POSITION')
  call geometry(m)
case('POSITIONS',"SITES")
  write (liw,"(/2A)") "Site positions for molecule ", trim(name(head(m)))
  debug(7)=.true.
  call axes(m,.false.)
  debug(7)=.false.
case("GEOMETRY","GEOMETRIES")
  call axes(m,.true.)
case("MULTIPOLES","MOMENTS")
  call axes(m,.false.)
  write (liw,'(/3a)') 'Total moments for molecule ',              &
      trim(name(head(m))),                                        &
      ', referred to molecular axes and origin'
  call totalq(m,qt,.true.)
  call printq(qt,4,                                               &
      '(t5, a, t18, 5f12.6:/1x,t18,4f12.6)')
case('BONDS')
  !  Print positions and bonds in format suitable for Estgen
  !  Note that atoms have to be numbered from 1. We assume that they are
  !  consecutive in the Orient file.
  call axes(m,.true.)
  write (liw,"(/A/)") name(head(m))
  base=head(m)
  k=next(base)
  do while (k .ne. 0)
    write (*,"(I3, 2x, A8, I3, 2X, 3F10.4)",advance="no")         &
        k-base, name(k), atnum(type(k)), sx(1:3,k)*rfact
    if (type(k) .ne. 0) then
      j=next(head(m))
      do while (j .ne. 0)
        if (j .ne. k .and. type(j) .ne. 0) then
          if ((sx(1,j)-sx(1,k))**2+(sx(2,j)-sx(2,k))**2           &
              +(sx(3,j)-sx(3,k))**2 .lt.                          &
              1.4*(radius(type(j))+radius(type(k)))**2) then
            write (*,"(I4)",advance="no") j-base
          endif
        endif
        j=next(j)
      end do
    endif
    write (*,"(A)") " "
    k=next(k)
  end do
case ("DATA")
  call punchm(m,unit=liw)
case ("DETAILS")
  call show_molecule_data(m)
case ("")
  call die                                                     &
      ("Keyword omitted following SHOW ALL or SHOW <molecule>",.true.)
case default
  call die                                                     &
      ("Unrecognized keyword after SHOW ALL or SHOW <molecule>",.true.)
end select

END SUBROUTINE show_molecule

SUBROUTINE show_molecule_data(m)

USE molecules, ONLY: head
USE sites, ONLY: name

INTEGER, INTENT(IN) :: m
INTEGER :: k

write (liw,"(/2a)") "Molecule ", trim(name(head(m)))
write (liw,"(a)") "Sites:"
k=head(m)
if (cm(m)>0) k=cm(m)
do
  write (liw,"(i4,2x,a)") k, trim(name(k))
  k=next(k)
  if (k == 0) exit
end do
write (liw,"(a, 3f8.4)") "Centre of mass position: ", mol(m)%cm
write (liw,"(a, 3f8.4)") "Angle-axis vector:       ", mol(m)%p
if (fixed(m)) then
  write (liw,"(a)") "Fixed"
end if
if (calc(m)) then
  write (liw,"(a)") "Site positions have been calculated"
else
  write (liw,"(a)") "Site positions have not been calculated"
end if


END SUBROUTINE show_molecule_data

END SUBROUTINE show

SUBROUTINE punchm(m,unit)

!  Output for subsequent re-input.

USE output
USE sites
USE sizes
USE types
USE molecules
USE variables
IMPLICIT NONE

INTEGER, INTENT(IN) :: m
INTEGER, INTENT(IN), OPTIONAL :: unit

LOGICAL :: first
CHARACTER(LEN=20) ::  ww
INTEGER :: i, k, u, rank

! iw_save = iw
! if (present(file)) then
!   iw = find_io(17)
!   open(unit=iw,file=filename,iostat=k)
!   if (k>0) then
!     print "(3a)", "Can't open ", trim(file), ". Using STDOUT."
!     iw=iw_save
!   end if
!   call put_params(iw)
! end if

if (present(unit)) then
  u = unit
else
  u = iw
end if

k=head(m)

first=.true.
do while (k .ne. 0)
  if (iand(sf(k),16) == 16) then
    ww="Polar"
  else
    ww=" "
  endif
  call putout(1)
  if (first) then
    call put_unit(u)
    write (u,"(2a)") "Units ", runit
    call putstr("Molecule",1)
  end if
  call putstr(name(k),1)
  call putstr(" at "//ww,1)
  do i=1,3
    call vout(sp(i,k))
  end do
  if (any(abs(q(17:25,k)) > 5d-7)) then
    rank = 4
  else if (any(abs(q(10:16,k)) > 5d-7)) then
    rank = 3
  else if (any(abs(q(5:9,k)) > 5d-7)) then
    rank = 2
  else if (any(abs(q(2:4,k)) > 5d-7)) then
    rank = 1
  else
    rank = 0
  end if
  
  if (.not. first) then
    if (link(k) .ne. head(m)) then
      call putstr(" relative to "//name(link(k)),0)
    end if
    call putstr(" Rank",1)
    call puti(rank,"(i4)",.true.)
  end if
  if (type(k) > 0) then
    call putstr(" Type",1)
    call putstr(tname(type(k)),0)
  endif
  if (iand(sf(k),7) > 0) then
    call putstr(" +++",0)
    call putout(0)
    call puttab(10)
  endif
  if (iand(sf(k),1) == 1) then
    call putstr(" Rotated",0)
    do i=1,3
      call vout(sr(i,k))
    end do
    call putsp(1)
  else if (iand(sf(k),2) == 2) then
    call putstr(" Rotated by",0)
    call vout(sr(1,k))
    call putstr(" about",0)
    do i=2,4
      call vout(sr(i,k))
    end do
    call putsp(1)
  endif
  if (iand(sf(k),4) == 4) then
    call putstr(" Twist",0)
    call vout(st(k))
  endif
  call putout(0)

  if (l(k) .ge. 0) then
    if (rank .ge. 0) write (u,"(f11.6)") q(1,k)
    if (rank .ge. 1) write (u, "(3f11.6)") q(2:4,k)
    if (rank .ge. 2) write (u, "(5f11.6)") q(5:9,k)
    if (rank .ge. 3) write (u, "(7f11.6)") q(10:16,k)
    if (rank .ge. 4) write (u, "(7f11.6/(11x,4f11.6))") q(17:25,k)
  endif
  first=.false.
  k=next(k)
end do
write (u,"(/a//a)") "End", "Units pop"

END SUBROUTINE punchm

END MODULE show_data

