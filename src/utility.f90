MODULE utility

IMPLICIT NONE

PRIVATE
PUBLIC strf, stri, matwrt, matwrtv, matwrtl, matwrtll

INTERFACE matwrt
  MODULE PROCEDURE matwrt_dp, matwrt_i
END INTERFACE

INTEGER, PARAMETER :: dp=kind(1d0)

CONTAINS

!----------------------------------------------------------------

!  Return a character string representation of the real*8 number F,
!  with leading spaces stripped if LJ is .TRUE.
FUNCTION strf(f,format,lj)
IMPLICIT NONE
CHARACTER(LEN=*) :: format
CHARACTER(LEN=20) :: buffer, strf
DOUBLE PRECISION f
INTEGER l
LOGICAL lj

write (unit=buffer,fmt=format) f
l=1
if (lj) then
  do while (buffer(l:l) .eq. ' ' .and. l .lt. 20)
    l=l+1
  end do
endif
strf=buffer(l:)
return
END FUNCTION strf

!----------------------------------------------------------------

!  Return a character string representation of the integer I,
!  with leading spaces stripped if LJ is .TRUE. or absent
CHARACTER(LEN=20) FUNCTION stri(i,fmt,lj)
IMPLICIT NONE
INTEGER, INTENT(IN) :: i
CHARACTER(LEN=*), INTENT(IN), OPTIONAL :: fmt
LOGICAL, INTENT(IN), OPTIONAL :: lj
CHARACTER(LEN=20) :: buffer, format

if (present(fmt)) then
  format=fmt
else
  format="(i12)"
endif
write (unit=buffer,fmt=format) i
if (present(lj)) then
  if (lj) then
    stri=adjustl(buffer)
  else
    stri=buffer
  endif
else
  stri=adjustl(buffer)
endif

END FUNCTION stri

!=======================================================================

SUBROUTINE matwrt_dp(c,unit,ncols,fmt,label)

!  Print matrix C on unit UNIT in
!  NCOLS columns. The numbers are printed with format fmt. The arguments
!  unit, ncols and fmt are optional; if they are omitted, default values
!  are used: unit=6, ncols=6, fmt = "1p,6g12.4". If the label argument
!  is present, the rows and columns are labelled, with integer values
!  starting at the value of label.

IMPLICIT NONE
REAL(dp), INTENT(IN) :: c(:,:)
INTEGER, INTENT(IN), OPTIONAL :: unit, ncols
CHARACTER(LEN=*), INTENT(IN), OPTIONAL :: fmt
INTEGER, INTENT(IN), OPTIONAL :: label

INTEGER rows, cols
CHARACTER(LEN=40) :: format
INTEGER iw, i, j, jstart, jfinis, nc

rows=size(c,1)
cols=size(c,2)
if (present(unit)) then
  iw=unit
else
  iw=6
endif
if (present(ncols)) then
  nc=ncols
else
  nc=6
endif
if (present(fmt)) then
  format="("//fmt//")"
else
  format="(1p,6g12.4)"
endif
jfinis=0
do while (jfinis < cols)
  jstart=jfinis+1
  jfinis=min(cols,jfinis+nc)
  if (present(label)) then
    write (iw,'(/5x,12(4x,i4,4x))') (label+j-1,j=jstart,jfinis)
    write (iw,'(1x)')
  end if
  do i=1,rows
    if (present(label)) write (iw,"(1x,i3,1x)",advance="no") label+i-1
    write (iw,format) (c(i,j),j=jstart,jfinis)
  end do
end do

END SUBROUTINE matwrt_dp

!=======================================================================

SUBROUTINE matwrt_i(c,unit,ncols,fmt,label)

!  Print integer matrix C on unit UNIT in
!  NCOLS columns. The numbers are printed with format fmt. The arguments
!  unit, ncols, fmt and label are optional; if they are omitted, default
!  values are used: unit=6, ncols=25, fmt = "(25i4)". If the label argument
!  is present, the rows and columns are labelled, with integer values
!  starting at the value of label.

IMPLICIT NONE
INTEGER, INTENT(IN) :: c(:,:)
INTEGER, INTENT(IN), OPTIONAL :: unit, ncols
CHARACTER(LEN=*), INTENT(IN), OPTIONAL :: fmt
INTEGER, INTENT(IN), OPTIONAL :: label

INTEGER rows, cols
CHARACTER(LEN=40) :: format
INTEGER iw, i, j, jstart, jfinis, nc

rows=size(c,1)
cols=size(c,2)
if (present(unit)) then
  iw=unit
else
  iw=6
endif
if (present(ncols)) then
  nc=ncols
else
  nc=25
endif
if (present(fmt)) then
  format="("//fmt//")"
else
  format="(25i4)"
endif
jfinis=0
do while (jfinis < cols)
  jstart=jfinis+1
  jfinis=min(cols,jfinis+nc)
  if (present(label)) then
    write (iw,"(5x,"//fmt//")") (label+j-1,j=jstart,jfinis)
    write (iw,'(1x)')
  end if
  do i=1,rows
    if (present(label)) write (iw,"(1x,i3,1x)",advance="no") label+i-1
    write (iw,format) (c(i,j),j=jstart,jfinis)
  end do
end do

END SUBROUTINE matwrt_i

!=======================================================================

SUBROUTINE matwrtv(e,c,i1,i2,j1,j2,unit,ncols,fmte,fmtc)

!  Print rows I1 to I2, columns J1 to J2, of matrix C on unit IW,
!  each column headed by the corresponding element of the vector E.
!  The elements of E are printed using format fmte, if provided,
!  otherwise with format "12f12.6". The elements of C are printed with
!  format fmtc if provided, otherwise with format "12f12.6".
!  The numbers are printed in ncols columns, default value 6.

IMPLICIT NONE
DOUBLE PRECISION, INTENT(IN) :: c(:,:),e(:)
INTEGER, INTENT(IN) :: i1, i2, j1, j2
INTEGER, INTENT(IN), OPTIONAL :: unit, ncols
CHARACTER(LEN=*), OPTIONAL :: fmte,fmtc
CHARACTER(LEN=50) :: formate, formatc
INTEGER iw, i, j, jstart, jfinis, ncol

if (present(unit)) then
  iw=unit
else
  iw=6
endif
if (present(ncols)) then
  ncol=ncols
else
  ncol=6
endif
if (present(fmte)) then
  formate="(/3x,"//fmte//")"
else
  formate="(/3x,12f12.6)"
endif
if (present(fmtc)) then
  formatc="(/i3,"//fmtc//")"
else
  formatc="(/i3,12f12.6)"
endif

jfinis=j1-1
do while (jfinis < j2)
  jstart=jfinis+1
  jfinis=min(j2,jfinis+ncol)
  write (iw,formate) (e(j),j=jstart,jfinis)
  write (iw,'(1x)')
  do i=i1,i2
    write (iw,formatc) i, (c(i,j),j=jstart,jfinis)
  end do
end do

END SUBROUTINE matwrtv

!=======================================================================

SUBROUTINE matwrtl(label,e,c,i1,i2,j1,j2,unit,ncols,fmte,fmtc)

!  Print rows I1 to I2, columns J1 to J2, of matrix C on unit UNIT,
!  each column headed by the corresponding element of the vector E,
!  each row preceded by the corresponding element of the vector label.
!  The elements of E are printed using format fmte, if provided,
!  otherwise with format "12f12.6". The elements of C are printed with
!  format fmtc if provided, otherwise with format "12f12.6".
!  The numbers are printed in ncols columns, default value 6.

IMPLICIT NONE
DOUBLE PRECISION, INTENT(IN) :: c(:,:),e(:)
INTEGER, INTENT(IN) :: i1, i2, j1, j2
CHARACTER(LEN=*), INTENT(IN) :: label(:)
INTEGER, INTENT(IN), OPTIONAL :: unit, ncols
CHARACTER(LEN=*), INTENT(IN), OPTIONAL :: fmte,fmtc

CHARACTER(LEN=50) :: formate, formatc
integer iw, i, j, jstart, jfinis, ncol

if (present(unit)) then
  iw=unit
else
  iw=6
endif
if (present(ncols)) then
  ncol=ncols
else
  ncol=6
endif
if (present(fmte)) then
  formate="(/8x,"//fmte//")"
else
  formate="(/8x,12f12.6)"
endif
if (present(fmtc)) then
  formatc="(A,"//fmtc//")"
else
  formatc="(A,12f12.6)"
endif

jfinis=j1-1
do while (jfinis .lt. j2)
  jstart=jfinis+1
  jfinis=min(j2,jfinis+ncol)
  write (iw,formate) (e(j),j=jstart,jfinis)
  write (iw,'(1x)')
  do i=i1,i2
    write (iw,formatc) adjustr(label(i)),(c(i,j),j=jstart,jfinis)
  end do
end do

END SUBROUTINE matwrtl

!=======================================================================

SUBROUTINE matwrtll(c,unit,ncols,fmtc,                     &
    rowlabel,collabel,rowlabelwidth,collabelwidth)

!  Print rows I1 to I2, columns J1 to J2, of matrix C on unit UNIT,
!  each column headed by the corresponding element of the vector 
!  collabel, if provided, and each row preceded by the corresponding
!  element of the vector rowlabel. rowlabelwidth and collabelwidth are
!  optional CHARACTER arguments specifying the width of the labels
!  (default "8" and "12" respectively).
!  The elements of C are printed with
!  format fmtc if provided, otherwise with format "12f12.6".
!  The numbers are printed in ncols columns, default value 6.

IMPLICIT NONE
DOUBLE PRECISION, INTENT(IN) :: c(:,:)
INTEGER, INTENT(IN), OPTIONAL :: unit, ncols
CHARACTER(LEN=*), INTENT(IN), OPTIONAL ::                              &
    fmtc, rowlabel(:), collabel(:), rowlabelwidth, collabelwidth

CHARACTER(LEN=50) :: formatc
CHARACTER(LEN=3) :: rlw, clw
INTEGER :: iw, i, j, jstart, jfinis, ncol
INTEGER :: i2, j2

if (present(unit)) then
  iw=unit
else
  iw=6
endif
if (present(ncols)) then
  ncol=ncols
else
  ncol=6
endif
if (present(rowlabelwidth)) then
  rlw=rowlabelwidth
else if (present(rowlabel)) then
  rlw="8"
else
  rlw="0"
endif
if (present(collabelwidth)) then
  clw=collabelwidth
else
  clw="12"
endif
if (present(fmtc)) then
  formatc="("//fmtc//")"
else
  formatc="(12f12.6)"
endif

i2=size(c,1)
j2=size(c,2)

jfinis=0
do while (jfinis .lt. j2)
  jstart=jfinis+1
  jfinis=min(j2,jfinis+ncol)
  if (present(collabel)) then
    write (iw,"(/,"//rlw//"x,50a"//clw//")") (collabel(j),j=jstart,jfinis)
  end if
  write (iw,'(1x)')
  do i=1,i2
    if (present(rowlabel)) then
      write (iw,"(a"//rlw//")",advance="no") rowlabel(i)
    endif
    write (iw,formatc) (c(i,j),j=jstart,jfinis)
  end do
end do

END SUBROUTINE matwrtll

END MODULE utility
