MODULE gl_module

#ifdef F2003
! USE, INTRINSIC :: ISO_C_BINDING, ONLY : C_CHAR, C_NULL_CHAR
USE f03gl_kinds
USE f03gl_gl
USE f03gl_glu
USE f03gl_glut
#else
USE opengl_gl
USE opengl_glu
USE opengl_glut
#endif

USE types, ONLY : tcolour
USE sites, ONLY : sx, type
USE input, ONLY : die
USE rotations, ONLY: angleaxis

IMPLICIT NONE

PRIVATE
PUBLIC :: gl_init, title, point, bond, nb, points, triangles, height, &
    scale, scale_width, colour, lastcolour, bg, triangle_index, pt,   &
    show_atom, show_vector, show_scalar, show_energy, show_colour_scale, &
    x_left, x_right, y_bottom, y_top, z_near, z_far, x_reset, vecmax, &
    Vmin, Vmax, Vtop, Vbottom, tick, ticks, contour_levels, contour,  &
    MAXCOL, p2p_pts, p2p_grid, use_lighting, lighting_spec, material_spec, &
    lmodel_ambient, light, material, view0, video, video_prefix,      &
    video_frames

INTEGER, PARAMETER :: dp=kind(1d0), MAXCOL=50
DOUBLE PRECISION, PARAMETER :: pi=3.14159265358979d0
CHARACTER(LEN=80) :: title=""
#ifdef F2003
! CHARACTER(KIND=C_CHAR), TARGET :: title_string(80)
#endif

TYPE point
  REAL :: x(3)       !  Position
  REAL :: v(0:3)     !  Scalar and vector function values
  REAL :: n(3)       !  Direction of outward normal
  REAL :: colour(4)  !  Colour and alpha
  INTEGER :: site    !  Site associated with this point (if any)
END TYPE point

! TYPE(point), SAVE :: pt(10000)
TYPE(point), ALLOCATABLE, SAVE :: pt(:)
INTEGER :: points=0

! REAL :: site(3,20)=0.0
! CHARACTER(LEN=20) :: site_name(20)
INTEGER :: nb=0
INTEGER, ALLOCATABLE :: bond(:,:)
LOGICAL :: show_vector=.false., show_scalar=.false.,                 &
    show_energy=.false., reverse=.false. ! , show_site(20)=.true.

!  Viewport: the displayed picture extends from x_left to x_right in x,
!  and correspondingly in y, according to the window size.
DOUBLE PRECISION :: x_reset=10d0, x_left=-10d0, x_right=10d0,          &
    y_bottom=-10d0, y_top=10d0, z_near=10d0, z_far=-10d0

!  triangle_index(v,n) = point v (v=1,2,3) of triangle n, with position
!  pt(v)%x, colour pt(v)%colour, etc.
INTEGER, ALLOCATABLE :: triangle_index(:,:)
INTEGER :: triangles

REAL, SAVE :: vecmax=0.0  ! Maximum vector magnitude

INTEGER :: contour_levels=0
DOUBLE PRECISION :: contour(10)

INTEGER :: p2p_pts=0
DOUBLE PRECISION, ALLOCATABLE :: p2p_grid(:,:)

! LOGICAL :: debug(7)=.false.

!  Scale factor for field vectors
REAL :: scale=200.0, scale_save, scale_reset

INTEGER(GLenum) :: doubleBuffer=GL_TRUE, mode
INTEGER(GLint) :: top=10, left=10, width=548, height=500, windowid,    &
    mouse_x, mouse_y
LOGICAL :: video=.false.
INTEGER :: count=0, video_frames=0
CHARACTER(LEN=32) :: videofile="", video_prefix="frame"
INTEGER, POINTER :: videoframe(:)

! DOUBLE PRECISION :: viewpoint(3)=(/0d0,0d0,20d0/), viewpt(3)
    ! up(3)=(/0d0,1d0,0d0/), clip=1d0
DOUBLE PRECISION :: nx, ny, nz, m(4,4), view(3,3),                     &
    view0(3,3)=reshape([1d0,0d0,0d0,0d0,1d0,0d0,0d0,0d0,1d0],[3,3])

!  Spotfactor is used to increase the spot size for a clipped display.
REAL :: spot, spotsize=2.0! , &  ! pixels
!        spotfactor=1.0

!  Background colour
REAL(GLCLAMPF) :: bg(3)=(/1.0,1.0,1.0/)
!  Range of potentials corresponding to colour range
DOUBLE PRECISION :: Vmin=0d0, Vmax=0d0, Vtop=0d0, Vbottom=0d0, start, spread
DOUBLE PRECISION :: tick(10)=0d0
INTEGER :: lastcolour=0, ticks=0
REAL, SAVE :: colour(3,0:MAXCOL)=0.0
REAL :: mol_colour(4)


! LOGICAL :: static=.true.
LOGICAL :: rotating=.false., stepping=.false.
DOUBLE PRECISION :: psi=0d0, dpsi=1d0  !  Angles in degrees
LOGICAL :: show_colour_scale=.true.
INTEGER(GLINT) :: scale_width=48
LOGICAL, ALLOCATABLE :: show_atom(:)

INTEGER :: mouse_button=0
LOGICAL :: mouse_down=.false., reset=.true., hide=.false.

REAL :: temp(4)=0.0

!  Lighting
LOGICAL :: use_lighting=.false.
REAL :: white_light(4)=[1.0,1.0,1.0,1.0]
REAL :: dim_light(4)=[0.2,0.2,0.2,1.0]
REAL :: lmodel_ambient(4)  ! =[0.2,0.2,0.2,1.0]
TYPE material_spec
  REAL :: ambient(4)  ! =[1.0,1.0,1.0,1.0]
  REAL :: diffuse(4)  ! =[1.0,1.0,1.0,1.0]
  REAL :: specular(4)  ! =[1.0,1.0,1.0,1.0]
  REAL :: shiny  ! =30.0
END type material_spec
TYPE lighting_spec
  CHARACTER(LEN=6) :: lightname
  LOGICAL :: enabled=.false.
  !  Directional light source (last parameter zero)
  REAL :: position(4)  ! =[1.0,1.0,1.0,0.0]
  REAL :: diffuse(4)  ! =[0.2,0.2,0.2,1.0]
  REAL :: ambient(4)  ! =[0.1,0.1,0.1,1.0]
  REAL :: specular(4)  ! =[1.0,1.0,1.0,1.0]
END type lighting_spec

TYPE(material_spec) :: material
TYPE(lighting_spec), SAVE :: light(0:7)

CONTAINS

!-----------------------------------------------------------------------

SUBROUTINE GL_init

CHARACTER(LEN=*), PARAMETER :: this_routine="show_map"

print "(2a)", "Entering ", this_routine

! if (allocated(pt)) then
!   print "(a,i0)", "pt array is allocated with size ", size(pt)
! else
!   print "(a)", "pt array is not allocated"
! end if

! call glGetFloatv(GL_SMOOTH_POINT_RANGE,temp)
! print "(a,2f6.2)", "Smooth point size range: ", temp(1:2)
call glutinit()
call glutInitWindowPosition(top,left)
if (show_colour_scale) then
  width=height+scale_width
else
  scale_width=0
  width=height
end if
call glutInitWindowSize(width,height)
  mode = ior(GLUT_RGB,GLUT_DEPTH) 
  if (doubleBuffer == GL_TRUE) then
     mode = ior(mode,GLUT_DOUBLE)
  else
     mode = ior(mode,GLUT_SINGLE)
  end if
call glutInitDisplayMode( mode )

#ifdef F2003
! print "(a)", "Creating window"
windowid=glutCreateWindow(CString(trim(title)))
! print "(a)", "Window created"
#else
windowid=glutCreateWindow(trim(title))
#endif

call glClearColor(bg(1),bg(2),bg(3),0.0)
call glClearDepth(1d0)
call glEnable(GL_DEPTH_TEST)
! call glEnable(GL_FOG)
call glEnable(GL_POINT_SMOOTH)
! call glHint(GL_FOG_HINT,GL_NICEST)

call glutWindowStatusFunc(Visible)
call glutDisplayFunc(show)
call glutKeyboardFunc(Keyboard)
call glutMouseFunc(mouse)
call glutMotionFunc(drag)
call glutReshapeFunc(resize)

call glEnable(GL_NORMALIZE)

if (vecmax == 0.0) then
  scale = 200.0
else
  scale = 5d0/vecmax
end if
scale_save=scale
scale_reset=scale
view=view0

if (video) then
  rotating=.true.
  psi=0d0
  dpsi=-360/video_frames   !  Angles in degrees
  count=0
  !  width*height pixels, 3 bytes each, packed in 4-byte integers
  allocate(videoframe(width*height*3/4))
end if

! print "(2a)", "Calling glutMainLoop"
call glutMainLoop()

END SUBROUTINE GL_init

!-----------------------------------------------------------------------

#ifdef F2003
SUBROUTINE show() BIND(C,NAME="")
#else
SUBROUTINE show
#endif

INTEGER :: i, ok
LOGICAL :: first=.true.

! print "(a,i0)", "Show: width = ", width
spot=spotsize/width

call glClear(ior(GL_COLOR_BUFFER_BIT,GL_DEPTH_BUFFER_BIT))

call colour_strip

if (reset) then
  m=0d0
  m(1:3,1:3)=view
  m(4,4)=1d0
  reset=.false.
  call glMatrixMode(GL_MODELVIEW)
  call glLoadIdentity
end if

!  View for molecule

if (use_lighting) then
  if (first) then
    print "(a10,4f8.2)", "Ambient", material%ambient, &
        "Diffuse", material%diffuse, "Specular", material%specular, &
        "Shininess", material%shiny
    print "(a10,4f8.2)", "Position", light(0)%position, "Ambient", light(0)%ambient, &
        "Diffuse", light(0)%diffuse, "Specular", light(0)%specular
    first=.false.
  end if

  call glEnable(GL_LIGHTING)
  call glLightModelfv(GL_LIGHT_MODEL_AMBIENT,lmodel_ambient)
  call glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL,GL_SEPARATE_SPECULAR_COLOR)

  call glMatrixMode(GL_MODELVIEW)
  call glMaterialfv(GL_FRONT,GL_AMBIENT,material%ambient)
  call glMaterialfv(GL_FRONT,GL_DIFFUSE,material%diffuse)
  call glMaterialfv(GL_FRONT,GL_SPECULAR,material%specular)
  call glMaterialf(GL_FRONT,GL_SHININESS,material%shiny)
  call glEnable(GL_COLOR_MATERIAL)
  call glColorMaterial(GL_FRONT,GL_DIFFUSE)
  call glColorMaterial(GL_FRONT,GL_AMBIENT)

  call glMatrixMode(GL_MODELVIEW)
  call glLoadIdentity
  call glLightfv(GL_LIGHT0, GL_POSITION, light(0)%position)
  call glLightfv(GL_LIGHT0,GL_DIFFUSE,light(0)%diffuse)
  call glLightfv(GL_LIGHT0,GL_AMBIENT,light(0)%ambient)
  call glLightfv(GL_LIGHT0,GL_SPECULAR,light(0)%specular)
  if (light(0)%enabled) then
    call glEnable(GL_LIGHT0)
  else
    call glDisable(GL_LIGHT0)
  end if

else
  call glDisable(GL_LIGHTING)
end if

call glEnable(GL_BLEND)

! call glMatrixMode(GL_PROJECTION)
call glMatrixMode(GL_MODELVIEW)
if (rotating) then
  psi=psi+dpsi
else if (stepping) then
  psi=psi+10d0*dpsi
end if
m(1:3,1:3)=view
call glLoadMatrixd(m)
call glRotated(psi,view(2,1),view(2,2),view(2,3))
stepping=.false.
call glViewport(scale_width,0,width-scale_width,height)
call glOrtho(x_left,x_right,y_bottom,y_top,z_near,z_far)

! call glFogi(GL_FOG_MODE,GL_EXP)
! call glFogfv(GL_FOG_COLOR,(/1.0,1.0,1.0,0.0/))
! call glFogf(GL_FOG_DENSITY,0.5)
! call glFogf(GL_FOG_START,real(d-1d0,GLFLOAT))
! call glFogf(GL_FOG_END,real(d+1d0,GLFLOAT))


mol_colour(4)=0.75
call glBlendFunc(GL_ONE,GL_ZERO)
!  print "(a)", "ball-and-stick"
call ball_and_stick

if (.not. hide) then
  if (.not. (show_scalar .or. show_energy .or. show_vector)) then
    print "(a)", "No scalar or vector field specified -- don't know what to do"
    stop
  end if
  call glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA)
  call surface
  if (show_vector) then
    call field
  end if
  if ((show_scalar .or. show_energy) .and. contour_levels>0) then
    call contours
  end if
end if

if (p2p_pts>0) then
  call glPointSize(2.0*spotsize)
  call glEnable(GL_POINT_SMOOTH)
  call glColor4f(0.0,0.0,0.0,0.5)
  call glBegin(GL_POINTS)
  do i=1,p2p_pts
    call glVertex3dv(p2p_grid(:,i))
  end do
  call glEnd
end if


if (doubleBuffer == GL_TRUE) then
  if (video) then
    count=count+1
    write (videofile,fmt="(3a,i3.3,a)") "video/", trim(video_prefix),  &
        "_", count, ".raw"
    inquire(iolength=i) videoframe
    open (43,file=videofile,form="unformatted", recl=i, iostat=ok)
    if (ok>0) call die ("Can't open file "//trim(videofile),.true.)
    call glReadBuffer(GL_BACK)
    call glReadPixels(0,0,width,height,GL_RGB,                         &
        GL_UNSIGNED_BYTE, c_loc(videoframe(1)))
    write (43) videoframe(:)
    close(43)
    ! print "(2a,f8.3)", videofile, "psi = ", psi
    if (count>=video_frames) then
      video=.false.
      rotating=.false.
      !  call glutLeaveMainLoop
      !  return
    end if
  end if
  call glutSwapBuffers()
else
  call glFlush()
end if

if (rotating) then
  call glutPostRedisplay()
end if

END SUBROUTINE show

!-----------------------------------------------------------------------

SUBROUTINE whenidle() BIND(C,NAME="")

! if (rotating .and. .not. video) then
  ! psi=psi+pi/180d0
!   call glutPostRedisplay()
! end if

END SUBROUTINE whenidle

!-----------------------------------------------------------------------

!  SUBROUTINE Keyboard

#ifdef F2003
SUBROUTINE Keyboard(key, x, y) BIND(C,NAME="")
INTEGER(GLbyte), VALUE :: key
INTEGER(GLint), VALUE :: x, y
#else
SUBROUTINE Keyboard(key, x, y)
INTEGER(GLint), INTENT(IN OUT) :: key, x, y
#endif

!  Keyboard display controls. Letters may be upper or lower case.
!  Q or ctrl-Q : exit
!  H  hide map (just show ball-and-stick model)
!  L  toggle lighting (but lighting doesn't work correctly at present)
!  M  print out rotation matrix and angle-axis rotation for current view.
!  R  reset to original view (no effect if rotating)
!  S  start or stop rotation
!  T  stepwise rotation
!  U  reverse sense of surface triangles: normally the front face of each
!     triangle has the vertices of the triangle in counter-clockwise order. 
!  V  start video capture
!  >  speed up rotation by factor of 10
!  <  slow down rotation by factor of 10
!  If rotating:
!  *n speed up rotation by factor n (1 <= n <= 9)
!  /n slow down rotation by factor n (1 <= n <= 9)
!  -  change direction of rotation
!  If displaying vectors:
!  *n scale vector length by factor n (1 <= n <= 9)
!  /n scale vector length by factor 1/n (1 <= n <= 9)
!  -  reverse vector direction


INTEGER :: k
CHARACTER :: c
LOGICAL :: mult=.true.
DOUBLE PRECISION :: psi, p(3)

k=key
! print "(a,i0)", "Key value: ", k
if (k<32) then
  select case(k)
  case(17)
    ! ctrl-Q
    !  glutLeaveMainLoop should work in freeglut but doesn't. Not available
    !  at all in some versions of glut.
#ifdef OSX
    stop
#else
    call glutLeaveMainLoop
    return
    stop
#endif
  end select
else
  if (k > 96 .and. k < 123) k=k-32   !  Convert letters to upper case
  c=achar(k)
  select case(c)
  case("?")
    print "(a)",                                                         &
      "Keyboard display controls. Letters may be upper or lower case.", &
      "?  Show this list", &
      "Q or ctrl-Q : exit", &
      "F  Toggle display of vectors", &
      "H  hide map (just show ball-and-stick model)", &
      "L  toggle lighting (but lighting doesn't work correctly at present)", &
      "M  print out rotation matrix and angle-axis rotation for current view.", &
      "R  reset to original view (no effect if rotating)", &
      "S  start or stop rotation", &
      "T  stepwise rotation", &
      "U  reverse sense of surface triangles: normally the front face of each", &
      "   triangle has the vertices of the triangle in counter-clockwise order. ", &
      "V  Export video (see manual for details).", &
      ">  speed up rotation by factor of 10", &
      "<  slow down rotation by factor of 10", &
      "If rotating:", &
      "  *n speed up rotation by factor n (1 <= n <= 9)", &
      "  /n slow down rotation by factor n (1 <= n <= 9)", &
      "  -  change direction of rotation", &
      "If displaying vectors:", &
      "  *n scale vector length by factor n (1 <= n <= 9)", &
      "  /n scale vector length by factor 1/n (1 <= n <= 9)", &
      "  -  reverse vector direction"
  case("F")
    show_vector = .not. show_vector
    ! call glutPostRedisplay()
  case ("H")
    hide=.not. hide
    ! call glutPostRedisplay()
  case("L")
    !  Lighting currently doesn't work correctly
    use_lighting=.not. use_lighting
    light(0)%enabled=.not. light(0)%enabled
    call glutPostRedisplay()
  case("M")
    !  Print current rotation
    print "(4f10.5)", m
    call angleaxis(m(1:3,1:3),p)
    psi=sqrt(p(1)**2+p(2)**2+p(3)**2)
    p=p/psi
    psi=-psi*180/pi
    print "(a,f10.5,a,3f9.5)", "Rotated by ", psi, " about", p
  case ("Q")
#ifdef OSX
    stop
#else
    !  Should work in freeglut but doesn't. Not available at all in
    !  some versions of glut.
    call glutLeaveMainLoop
    return
    ! stop
#endif
  case ("R")        !  Reset
    reset=.true.
    psi=0d0
    view=view0
    x_right=x_reset
    x_left=-x_right
    y_top=(x_right*height)/(width-scale_width)
    y_bottom=-y_top
    z_far=x_left
    z_near=x_right
    scale=scale_reset
    rotating=.false.
    ! call glutPostRedisplay()
    ! static=.true.
    ! call glutIdleFunc()
  case ("S")        !  Stop or start rotation
    rotating=.not. rotating
    !   static=.not. static
    ! if (rotating) then
    !   call glutIdleFunc(whenidle)
    ! else
    !   call glutIdleFunc(GLUT_NULL_FUNC)
    ! endif
    ! call glutPostRedisplay()
  case("T")
    !  Step rotation
    stepping=.true.
    ! call glutPostRedisplay()
  case("U")
    reverse = .not. reverse
    ! call glutPostRedisplay()
  case("V")
    !  Export video (if set up)
    if (video_frames > 0) then
      video=.true.
      rotating=.true.
      psi=0d0
      dpsi=-360/video_frames   !  Angles in degrees
      count=0
      !  width*height pixels, 3 bytes each, packed in 4-byte integers
      allocate(videoframe(width*height*3/4))
      ! call glutPostRedisplay()
    end if
  case("-")
    scale=-scale
    dpsi=-dpsi
    ! call glutPostRedisplay()
  case("0")
    if (scale == 0.0) then
      scale=scale_save
    else
      scale_save=scale
      scale=0.0
    end if
    ! call glutPostRedisplay()
  case("*")
    mult=.true.
  case("/")
    mult=.false.
  case("1","2","3","4","5","6","7","8","9")
    if (rotating) then
      if (mult) then
        dpsi=dpsi*(k-48)
      else
        dpsi=dpsi/(k-48)
      end if
    else
      if (mult) then
        scale=scale*(k-48)
      else
        scale=scale/(k-48)
      end if
    end if
    ! call glutPostRedisplay()
  case("<")
    dpsi=dpsi/10d0
  case(">")
    dpsi=dpsi*10d0
  end select
end if

call glutPostRedisplay

END SUBROUTINE Keyboard

!-----------------------------------------------------------------------

#ifdef F2003
SUBROUTINE Visible(state) BIND(C,NAME="")
INTEGER(GLint), VALUE :: state
#else
SUBROUTINE Visible(state)
INTEGER(GLint) :: state
#endif

! if (state == GLUT_HIDDEN .or. state == GLUT_FULLY_COVERED) then
!   call glutIdleFunc(GLUT_NULL_FUNC)
! else
!   call glutIdleFunc(whenidle)
! end if

END SUBROUTINE Visible

!-----------------------------------------------------------------------

#ifdef F2003
SUBROUTINE resize(w, h) BIND(C,NAME="")
INTEGER(GLint), VALUE :: w, h
#else
SUBROUTINE resize(w, h)
INTEGER, INTENT(IN) :: w, h
#endif
! print "(a,i0,a,i0)", "Resizing to ", w, "x", h
width=w
height=h
y_top=(x_right*height)/(width-scale_width)
y_bottom=-y_top
reset=.true.
call glutPostRedisplay

END SUBROUTINE resize

!-----------------------------------------------------------------------

#ifdef F2003
SUBROUTINE mouse(b, state, px, py) BIND(C,NAME="")
INTEGER(GLint), VALUE :: b, state, px, py
#else
SUBROUTINE mouse(b, state, px, py)
INTEGER, INTENT(IN) :: b, state, px,py
#endif

! CHARACTER(LEN=4) :: up_or_down

if (state .eq. GLUT_DOWN) then
  mouse_down=.true.
  mouse_x=px
  mouse_y=py
  ! up_or_down="down"
else if (state .eq. GLUT_UP) then
  mouse_down=.false.
  ! up_or_down="up"
end if
select case(b)
case(GLUT_LEFT_BUTTON)
  mouse_button=1
case(GLUT_MIDDLE_BUTTON)
  mouse_button=2
case(GLUT_RIGHT_BUTTON)
  mouse_button=3
end select
! print "(a,i0,1x,a)", "Mouse button ",mouse_button, up_or_down

END SUBROUTINE mouse

!-----------------------------------------------------------------------

#ifdef F2003
SUBROUTINE drag(px, py) BIND(C,NAME="")
INTEGER(GLint), VALUE :: px, py
#else
SUBROUTINE drag(px, py)
INTEGER, INTENT(IN) :: px,py
#endif

DOUBLE PRECISION :: d, gamma, r(3,3)

if (px==mouse_x .and. py==mouse_y) return
nx=px-mouse_x
ny=py-mouse_y
mouse_x=px
mouse_y=py

if (mouse_button==1) then
  !  Rotate about axis in display plane, orthogonal to drag direction
  d=sqrt(nx**2+ny**2)
  nx=nx/d
  ny=ny/d
  nz=0d0
  gamma=-2d0*d/(width-scale_width)
  call rotate(gamma,[ny,nx,nz],r)
  view=matmul(r,view)
  m(1:3,1:3)=view
  call glMatrixMode(GL_MODELVIEW)
  ! call glRotated(gamma,ny,-nx,0d0)

else if (mouse_button==2) then
  !  Rotate about z (screen normal)
  gamma=atan2(mouse_y-ny-0.5d0*height,mouse_x-nx-0.5d0*(width-scale_width)) &
      -atan2(mouse_y-0.5d0*height,mouse_x-0.5d0*(width-scale_width))
  ! print "(a,f12.5)", "gamma = ", gamma
  call rotate(gamma,[0d0,0d0,1d0],r)
  view=matmul(r,view)
  m(1:3,1:3)=view
  call glMatrixMode(GL_MODELVIEW)

else if (mouse_button==3) then
  !  Increase or decrease image size
  x_right=max(1d0,x_right*(1d0-real(nx,dp)/(width-scale_width)))
  x_left=-x_right
  y_top=(x_right*height)/(width-scale_width)
  y_bottom=-y_top
  z_far=x_left
  z_near=x_right

endif

call glutPostRedisplay

END SUBROUTINE drag

!-----------------------------------------------------------------------

SUBROUTINE rotate(phi,n,r)

IMPLICIT NONE
DOUBLE PRECISION,INTENT(IN) :: phi, n(3)
DOUBLE PRECISION,INTENT(OUT) :: r(3,3)
DOUBLE PRECISION :: nx, ny, nz, g, s, ss

!  Construct OpenGL transformation matrix for a rotation through phi
!  about n. (It is not necessary to normalise n.)

g=sqrt(n(1)**2+n(2)**2+n(3)**2)
nx=n(1)/g; ny=n(2)/g; nz=n(3)/g
s=sin(phi)
ss=2d0*(sin(0.5*phi))**2
!  Note:  listed by rows
r(1,:) = [ 1d0-(ny**2+nz**2)*ss, nx*ny*ss-nz*s, nx*nz*ss+ny*s ]
r(2,:) = [ nx*ny*ss+nz*s, 1d0-(nx**2+nz**2)*ss, ny*nz*ss-nx*s ]
r(3,:) = [ nz*nx*ss-ny*s, ny*nz*ss+nx*s, 1d0-(nx**2+ny**2)*ss ]

END SUBROUTINE rotate

!-----------------------------------------------------------------------

SUBROUTINE colour_strip

INTEGER :: i, t
DOUBLE PRECISION :: y

call glDisable(GL_LIGHTING)

call glClearColor(bg(1),bg(2),bg(3),0.0)

!  Colour strip. Strip runs from Vtop to Vbottom. Regions between Vtop
!  and Vmax and between Vmin and Vbottom are in grey. Range between Vmax
!  and Vmin uses colour scale specified.
if (show_colour_scale) then
  call glDisable(GL_BLEND)
  call glLoadIdentity
  call glMatrixMode(GL_MODELVIEW)
  call glLoadIdentity
  call glViewport(0,0,scale_width,height)
  call glOrtho(0d0,1d0,0d0,10d0,-1d0,1d0)
  call glDisable(GL_CULL_FACE)

  !  Tick marks
  call glLineWidth(2.0)
  call glColor3f(0.0,0.0,0.0)
  do t = 1,ticks
    if (tick(t)>=Vbottom .and. tick(t)<=Vtop) then
      y = 10d0 * ((tick(t)-Vbottom)/(Vtop-Vbottom))
      call glBegin(GL_LINES)
      call glVertex3d(0d0,y,0.001d0)
      call glVertex3d(12d0,y,0.001d0)
      call glEnd
    end if
  end do

  call glBegin(GL_QUAD_STRIP)
  start=10d0*(Vmin-Vbottom)/(Vtop-Vbottom)
  spread=10d0*(Vmax-Vmin)/(Vtop-Vbottom)
  call glColor3f(0.75,0.75,0.75)
  call glVertex3d(0d0,0d0,0d0)
  call glVertex3d(1d0,0d0,0d0)
  call glVertex3d(0d0,start,0d0)
  call glVertex3d(1d0,start,0d0)
  do i=0,lastcolour
    call glColor3fv(colour(:,i))
    call glVertex3d(0d0,start+(spread*i)/lastcolour,0d0)
    call glVertex3d(1d0,start+(spread*i)/lastcolour,0d0)
  end do
  call glColor3f(0.75,0.75,0.75)
  call glVertex3d(0d0,start+spread,0d0)
  call glVertex3d(1d0,start+spread,0d0)
  call glVertex3d(0d0,10d0,0d0)
  call glVertex3d(1d0,10d0,0d0)
  call glEnd
end if

END SUBROUTINE colour_strip

!-----------------------------------------------------------------------

SUBROUTINE ball_and_stick

INTEGER i, j, k

!  Nuclei
! print "(a)", "Nuclei"
!: call glPointSize(8.0*spotsize)
!: call glColor3f(0.0,0.0,0.0)
!:call glBegin(GL_POINTS)
show_atom=.true.
do i=1,nb
  ! print "(2i2)", bond(:,i)
  do j=1,2
    k=bond(j,i)
    if (show_atom(k)) then
      mol_colour(1:3)=tcolour(:,type(k))
      call glColor4fv(mol_colour)
      !: call glVertex3dv(sx(:,k))
      call glPushMatrix
      call glTranslated(sx(1,k),sx(2,k),sx(3,k))
      call glutSolidSphere(0.5d0,20,16)
      call glPopMatrix
      show_atom(k)=.false.
    end if
  end do
end do
!: call glEnd
!!  show_atom=.true.
!!  do i=1,nb
!!    do j=1,2
!!      k=bond(j,i)
!!      if (show_atom(k)) then
!!        mol_colour(1:3)=tcolour(:,type(k))
!!        call glTranslated(sx(1,k),sx(2,k),sx(3,k))
!!        call glColor4fv(mol_colour)
!!        call glutSolidSphere(10d0,20,16)
!!        call glTranslated(-sx(1,k),-sx(2,k),-sx(3,k))
!!        show_atom(k)=.false.
!!      end if
!!    end do
!!  end do

!  Bonds
! print "(a)", "Bonds"
call glLineWidth(3.0)
call glBegin(GL_LINES)
do i=1,nb
  ! print "(2i2)", bond(:,i)
  mol_colour(1:3)=tcolour(:,type(bond(1,i)))
  call glColor4fv(mol_colour)
  call glVertex3dv(sx(:,bond(1,i)))
  call glVertex3dv(0.5d0*(sx(:,bond(1,i))+sx(:,bond(2,i))))
  mol_colour(1:3)=tcolour(:,type(bond(2,i)))
  call glColor4fv(mol_colour)
  call glVertex3dv(0.5d0*(sx(:,bond(1,i))+sx(:,bond(2,i))))
  call glVertex3dv(sx(:,bond(2,i)))
end do
call glEnd

END SUBROUTINE ball_and_stick

!-----------------------------------------------------------------------

SUBROUTINE surface

INTEGER :: i, t, v
  
if (use_lighting) then
  call glEnable(GL_LIGHTING)
  call glEnable(GL_LIGHT0)
  call glLightModelfv(GL_LIGHT_MODEL_AMBIENT,lmodel_ambient)
  call glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL,GL_SEPARATE_SPECULAR_COLOR)

  call glMatrixMode(GL_MODELVIEW)
  call glMaterialfv(GL_FRONT,GL_AMBIENT,material%ambient)
  call glMaterialfv(GL_FRONT,GL_DIFFUSE,material%diffuse)
  call glMaterialfv(GL_FRONT,GL_SPECULAR,material%specular)
  call glMaterialf(GL_FRONT,GL_SHININESS,material%shiny)
  ! call glEnable(GL_COLOR_MATERIAL)
  ! call glColorMaterial(GL_FRONT,GL_DIFFUSE)
  ! call glColorMaterial(GL_FRONT,GL_AMBIENT)

  ! call glLoadIdentity
  call glLightfv(GL_LIGHT0, GL_POSITION, light(0)%position)
  call glLightfv(GL_LIGHT0,GL_DIFFUSE,light(0)%diffuse)
  call glLightfv(GL_LIGHT0,GL_AMBIENT,light(0)%ambient)
  call glLightfv(GL_LIGHT0,GL_SPECULAR,light(0)%specular)
end if

!  This should be CCW, but for some reason that usually shows the
!  inside face, so CW is set instead. If that is wrong, use the "reverse"
!  command (U from keyboard) to change it to CW. Note that the display
!  doesn't change until the image is moved or reset (R).
if (reverse) then
  call glFrontFace(GL_CCW)
else
  call glFrontFace(GL_CW)
end if
call glEnable(GL_CULL_FACE)
call glCullFace(GL_BACK)
call glBegin(GL_TRIANGLES)
do t=1,triangles
  do i=1,3
    v=triangle_index(i,t)
    ! print "(2i5,2x,3f7.3,3x,4f6.3)", t, v, pt(v)%x, pt(v)%colour
    call glColor4fv(pt(v)%colour)
    call glVertex3fv(pt(v)%x)
    call glNormal3fv(pt(v)%n)
  end do
end do
call glEnd

END SUBROUTINE surface

!-----------------------------------------------------------------------

SUBROUTINE field

INTEGER :: i

!  Field points
call glPointSize(spotsize)
call glColor3f(0.0,0.0,0.0)
call glBegin(GL_POINTS)
! do i=1,points
!   ! if (show_site(pt(i)%site)) then
!   call glVertex3fv(pt(i)%x)
!   ! end if
! end do
call glEnd

!  Field vectors
call glLineWidth(1.5)
if (scale .ne. 0.0) then
  call glLineWidth(2.0)
  call glEnable(GL_LINE_SMOOTH)
  call glBegin(GL_LINES)
  do i=1,points
    ! if (show_site(pt(i)%site)) then
    !  Show only outward-pointing vectors (after scaling)
    if (scale*pt(i)%v(0) > 0.0) then
      call glColor4fv(pt(i)%colour)
      call glVertex3fv(pt(i)%x)
      call glVertex3fv((pt(i)%x+pt(i)%v(1:3)*scale))
    end if
    ! end if
  end do
  call glEnd
end if

END SUBROUTINE field

!-----------------------------------------------------------------------

SUBROUTINE contours

!  Draw contours on the surface at the levels specified in the
!  CONTOURS command.

INTEGER :: i, j, k, l, t, vx(3)
REAL :: c, u(3), xc(3,2)

call glLineWidth(1.5)
call glDisable(GL_BLEND)
call glEnable(GL_LINE_SMOOTH)
do l=1,contour_levels
  c=contour(l)
  if (c == 0.0) then
    call glColor4f(0.0,0.0,0.0,1.0)
  else if (c < 0.0) then 
    call glColor4f(0.0,0.0,1.0,1.0)
  else
    call glColor4f(1.0,0.0,0.0,1.0)
  end if
  call glBegin(GL_LINES)
  do t=1,triangles
    do i=1,3
      vx(i)=triangle_index(i,t)
    end do
    if (all(pt(vx(:))%v(0)<c) .or. all(pt(vx(:))%v(0)>c)) cycle
    k=0
    u(:)=pt(vx(:))%v(0)
    do i=1,3
      j=mod(i,3)+1
      if ( (u(i)-c)*(u(j)-c) < -0.001*(u(i)-u(j))**2) then
        k=k+1
        xc(:,k)=((u(j)-c)*(pt(vx(i))%x+0.01*pt(vx(i))%n)                 &
            +(c-u(i))*(pt(vx(j))%x+0.01*pt(vx(j))%n))/(u(j)-u(i))
      end if
    end do
    if (k .ne. 2) cycle !call die("Failure in contour setup")
    do k=1,2
      call glvertex3fv(xc(:,k))
    end do
  end do
  call glEnd
end do

END SUBROUTINE contours

END MODULE gl_module
