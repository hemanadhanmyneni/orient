SUBROUTINE hdiag (iegen,order,a,eivr,n)

IMPLICIT NONE

!  This routine uses a variable threshold Jacobi method.
!  It gives very good eigenvalues and eigenvectors.
!  The routine is much faster than the old HDIAG routine written
!  at M.I.T. that uses the Jacobi method but not the variable
!  threshold technique that is applied here.
!  This is JACVAT

!  Arguments:
!     IEGEN = .TRUE. provide eigenvectors, .FALSE. no eigenvectors
!     ORDER = .TRUE.: Arrange eigenvalues in ascending order
!     A      Array containing matrix to be diagonalized.
!            The eigenvalues are left on the diagonal of A.
!     EIVR   Array for eigenvectors
!     N      Size of matrix to diagonalize

LOGICAL, INTENT(IN) :: order, iegen
DOUBLE PRECISION :: a(:,:)
DOUBLE PRECISION, INTENT(OUT) :: eivr(:,:)
INTEGER, INTENT(IN) :: n

DOUBLE PRECISION, PARAMETER :: rthalf=0.7071067811865475244d0
DOUBLE PRECISION :: xpar1=1.0d-15,  xpar2=1.0d-18, aii, aij, ajj,      &
    amin, atop, avgf, d, dstop, c, s, t, thrsh, temp, u
INTEGER :: i, j, ii, jj, k, iflag, irow, jcol, nu


if (n .le. 1) then
  eivr(1,1)=1d0
  return
endif
if (iegen) then
  eivr=0d0
  do i=1,n
    eivr(i,i)=1d0
  end do
end if

!  Find the absolutely largest element of A

atop=0d0
do i=1,n
  do j=i,n
    atop=max(atop,abs(a(i,j)))
  end do
end do
if (atop .le. 0d0) return

!  Calculate the stopping criterion -- DSTOP

avgf=(n*(n-1))*0.55d0
d=0d0
do jj=2,n
  do ii=1,jj-1
    d=d+(a(ii,jj)/atop)**2
  end do
end do
dstop=xpar1*d

!  Calculate the threshold, THRSH

thrsh=sqrt(d/avgf)*atop

!  Start a sweep

iflag=1
do while (iflag>0)
  iflag=0
  do jcol=2,n
    do irow=1,jcol-1
      aij=a(irow,jcol)

      !  Compare the off-diagonal element with THRSH
      if (dabs(aij) .le. thrsh) cycle
      aii=a(irow,irow)
      ajj=a(jcol,jcol)
      s=ajj-aii

      !  Check to see if the chosen rotation is less than the rounding
      !  If so, then do not rotate.
      if (abs(aij) .lt. xpar2*abs(s)) cycle
      !	IF (DABS(AIJ) - 1.0D-11*DABS(S)) 130,130,118
      iflag=1

!  If the rotation is very close to 45 degrees, set sin and cos
!  to 1/(root 2).

      if (1d-10*dabs(aij).ge. dabs(s)) then
        s=rthalf
        c=s
      else

!  Calculation of sin and cos for rotation that is not very close
!  to 45 degrees

        t=aij/s
        s=0.25d0/sqrt(0.25d0+t*t)
!  cos = C,  sin = S
        c=dsqrt(0.5d0+s)
        s=2d0*t*s/c
      end if

!  Calculation of the new elements of matrix A

      do i=1,irow
        t=a(i,irow)
        u=a(i,jcol)
        a(i,irow)=c*t-s*u
        a(i,jcol)=s*t+c*u
      end do
      do i=irow+1,jcol-1
        t=a(i,jcol)
        u=a(irow,i)
        a(i,jcol)=s*u+c*t
        a(irow,i)=c*u-s*t
      end do
      a(jcol,jcol)=s*aij+c*ajj
      a(irow,irow)=c*a(irow,irow)-s*(c*aij-s*ajj)
      do j=jcol,n
        t=a(irow,j)
        u=a(jcol,j)
        a(irow,j)=c*t-s*u
        a(jcol,j)=s*t+c*u
      end do
      !  Rotation completed.

      !  See if eigenvectors are wanted by user
      if (iegen) then
        do i=1,n
          t=eivr(i,irow)
          eivr(i,irow)=c*t-eivr(i,jcol)*s
          eivr(i,jcol)=s*t+eivr(i,jcol)*c
        end do
      endif

      !  Calculate the new norm D and compare with DSTOP
      s=aij/atop
      d=d-s*s
      if (d<dstop) then
        !  Recalculate DSTOP and THRSH to discard rounding errors
        d=0d0
        do jj=2,n
          do ii=1,jj-1
            s=a(ii,jj)/atop
            d=s*s+d
          end do
        end do
        dstop=xpar1*d
      end if
      thrsh=dsqrt(d/avgf)*atop
    end do
  end do
end do  ! while iflag>0

if (order) then
  !  Arrange the eigenvalues in the order of increasing energy.
  !  Arrange the eigenvectors in the same order.
  nu=n
  do i=1,n
    amin=a(i,i)
    do j=i,nu
      if (a(j,j) .ge. amin) cycle
      ii=i
      amin=a(j,j)
      a(j,j)=a(i,i)
      a(i,i)=amin
      if (iegen) then
        do k=1,n
          temp=eivr(k,ii)
          eivr(k,ii)=eivr(k,j)
          eivr(k,j)=temp
        end do
      end if
    end do
  end do
end if

END SUBROUTINE hdiag
