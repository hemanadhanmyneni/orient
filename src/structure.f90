SUBROUTINE structure(m1,m2,qtn)

!  Express relative orientation of two molecules m1 and m2 in terms of
!  rotations relative to a local axis sytem with z axis from m1 to m2.

USE switches
USE consts
USE sites
USE molecules, ONLY: mol, head 
USE rotations, ONLY: quater
IMPLICIT NONE

INTEGER, INTENT(IN) :: m1, m2
LOGICAL, INTENT(IN) :: qtn

DOUBLE PRECISION :: rotate(3,3), quaternion(0:3), ab(3,3)
DOUBLE PRECISION :: alpha, beta, gamma, gpav2, gmav2, c, s, rr, zz, sinpsi, a1
INTEGER :: i, j, k, m

if (m1 .eq. m2) then
  print '(/A/)', 'STRUCTURE arguments equal'
  return
endif

!  Construct unit vector in direction from molecule m1 to molecule m2
ab=0d0
ab(:,3)=sx(:,head(m2))-sx(:,head(m1))
rr=sqrt(ab(1,3)**2+ab(2,3)**2+ab(3,3)**2)
ab(:,3)=ab(:,3)/rr
!     if (print .ge. 1) then
print '(/a,i0,a,i0,a,f12.4,1x,a)',                                     &
    'Molecules ', m1, ' and ', m2,                                     &
    ': intermolecular distance =', rr*rfact, runit
if (mol(m1)%mdim .gt. 0 .and. mol(m2)%mdim .gt. 0)                     &
    print '(a,i0,a,i0,a)',                                             &
    'Orientations relative to ', m1, '->', m2, ' vector: '
!     end if

!  Construct two more orthogonal vectors to complete rotation matrix
zz=ab(3,3)**2
if (zz > 0.5d0) then
!  First vector has a large Z component.
  ab(1,1)=1d0
else
  ab(3,1)=1d0
end if
call cross_product(ab(:,3),ab(:,1),ab(:,2))
call cross_product(ab(:,2),ab(:,3),ab(:,1))

! print  "(a/(3f10.5))", "Dimer axes", (ab(i,:),i=1,3)

m=m1
do
  ! print "(a,i0/(3f10.5))", "Molecule ", m, (se(i,:,head(m)),i=1,3)
  if (mol(m)%mdim .gt. 0) then
    do i=1,3
      do j=1,3
        rotate(i,j)=0.d0
        do k=1,3
          rotate(i,j)=rotate(i,j)+ab(k,i)*se(k,j,head(m))
        end do
        rotate(i,j)=min(rotate(i,j),1.d0)
        rotate(i,j)=max(rotate(i,j),-1.d0)
      end do
    end do

    ! print "(a/(3f10.5))", "Rotation matrix", (rotate(i,:),i=1,3)
    call quater(rotate,quaternion) ! quaternion parameters
    if (qtn) then
      write (6,'(a,i0,a,4f10.4)')                                      &
          'Quaternion parameters for molecule ', m, ': ', quaternion
      sinpsi=0d0
      do i=2,4
        sinpsi=sinpsi+quaternion(i)**2
      end do
      sinpsi=min(sqrt(sinpsi),1d0)
      if (sinpsi .gt. 1d-8) then
        write (6,'(a,f8.3,a,3f10.5)')                                  &
            'Rotated by', 2d0*asin(sinpsi)*afact,                      &
            ' about axis ', (quaternion(i)/sinpsi, i=2,4)
      endif
    endif
    s = sqrt(quaternion(1)**2+quaternion(2)**2)   ! = sin (beta/2)
    beta=2d0*asin(s)
    c = sqrt(1d0-s**2)          ! = cos (beta/2)
    if ( s < 1d-6 ) then
      gpav2=atan2(quaternion(3),quaternion(0))
      gmav2=0d0
    else if ( c < 1d-6 ) then
      gmav2=atan2(quaternion(1),quaternion(2))
      gpav2=0d0
    else
      gmav2=atan2(quaternion(1),quaternion(2))
      gpav2=atan2(quaternion(3),quaternion(0))
    endif
    alpha=gpav2-gmav2
    gamma=gpav2+gmav2
    if (m .eq. m1) then
      a1=alpha
      alpha=0d0
    else
      alpha=alpha-a1
      if (alpha .gt. pi) alpha=alpha-2d0*pi
      if (alpha .le. -pi) alpha=alpha+2d0*pi
    endif
    !       if (print .ge. 1) then
    write (6,'(a,i2,a,3F9.3)')                                         &
        'Euler angles for molecule', m, ': ',                          &
        alpha*afact, beta*afact, gamma*afact
    !       end if
  endif
  if (m .eq. m2) exit
  m=m2
end do

!----------------------------------------------------------------------

CONTAINS

SUBROUTINE cross_product(a,b,c)

!  c = normalized vector product of a and b

IMPLICIT NONE

DOUBLE PRECISION, INTENT(IN) :: a(3), b(3)
DOUBLE PRECISION, INTENT(OUT) :: c(3)
DOUBLE PRECISION :: s

c(1)=a(2)*b(3)-a(3)*b(2)
c(2)=a(3)*b(1)-a(1)*b(3)
c(3)=a(1)*b(2)-a(2)*b(1)
s=sqrt(c(1)**2+c(2)**2+c(3)**2)
if (s<1d-10) call die("Taking vector product of parallel vectors")
c=c/s

END SUBROUTINE cross_product

END SUBROUTINE structure
