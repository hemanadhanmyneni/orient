MODULE sizes

!  Number of elements in multipole array up to rank l is qsize(l)
INTEGER, PARAMETER :: qsize(-1:8) = [0,1,4,9,16,25,36,49,64,81]

!  Rank of element t in multipole array is lq(t)
INTEGER, PARAMETER :: lq(81) = [ 0, 1,1,1, 2,2,2,2,2, 3,3,3,3,3,3,3,   &
    4,4,4,4,4,4,4,4,4, 5,5,5,5,5,5,5,5,5,5,5,                          &
    6,6,6,6,6,6,6,6,6,6,6,6,6, 7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,          &
    8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8 ]

END MODULE sizes
