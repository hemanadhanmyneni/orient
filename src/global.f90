MODULE global

!  Various global variables
!  Input and output units (not used consistently)
INTEGER :: iw = 6, ir = 5

!  PREFIX, if not blank,  is prepended to any filename used by (some
!  parts of) the program.
CHARACTER*16, SAVE :: prefix=" "

LOGICAL :: tracing = .false.
LOGICAL :: crystal=.false., periodic=.false., pseudoperiodic=.false.
LOGICAL :: nanocrystal=.false., simulation=.false.

CONTAINS

!  Return a character string (filename) with a global prefix added

FUNCTION pfile(string)

CHARACTER(LEN=*) :: string
CHARACTER(LEN=40) :: pfile

if (prefix .eq.  ' ') then
  pfile=string
else
  pfile=trim(prefix)//string
endif

END FUNCTION pfile

END MODULE global
