MODULE variables

!  Module to handle variables and constants describing site positions
!  and orientations. 
!  VALUE is the numerical value of a constant or variable.
!  VNAME is the name of a variable or constant. (Most constants are
!  anonymous.)
!  VTYPE is 1 for a length, 2 for an angle, 0 for a dimensionless constant.
!  Angles and lengths are converted appropriately if read in or printed out.

USE consts
USE input, ONLY: read_line, item, nitems, reread, reada, readf, readi,  &
    upcase, readu, input_options, input_error_flag, die
IMPLICIT NONE

PRIVATE

INTEGER, SAVE, PUBLIC :: nval
!  NVAL is the maximum number of geometrical parameters (constants
!  and variables). Variables are allocated from the bottom upwards,
!  constants from the top downwards. NVAL is set at initialisation.
!  NVAR is the number of variables that have been defined, NVAL-NCONST
!  the number of constants.
INTEGER, SAVE, PUBLIC :: nvar=0, nconst, freek=0, nv=0
INTEGER, DIMENSION(:), ALLOCATABLE, SAVE, PUBLIC :: vtype
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE, SAVE, PUBLIC :: value, x0
DOUBLE PRECISION, ALLOCATABLE, PUBLIC :: vmap(:,:)

CHARACTER(LEN=12), DIMENSION(:), ALLOCATABLE, SAVE, PUBLIC :: vname

CHARACTER(LEN=50) :: string
CHARACTER(LEN=13) :: vartype(0:2)=(/"undimensioned","length       ","angle        "/)

PUBLIC alloc_variables, vardefs, readv, vvalue, setval, vprint, vslot, &
    vout, vary, findv, define_variables, variable, vlist

TYPE variable
  !  Type is 1 for a length variable, 3 for a rotation
  INTEGER type, index
  CHARACTER(LEN=16) :: name
  DOUBLE PRECISION :: x=0d0, p(3)=[0d0,0d0,0d0]
  TYPE(variable), POINTER :: next=>null()
END type variable
TYPE(variable), POINTER :: vlist=>null(), lastvar=>null()

CONTAINS

SUBROUTINE alloc_variables

INTEGER :: ok

nconst=nval
allocate(vname(0:nval), value(0:nval), vtype(0:nval), stat=ok)
if (ok .gt. 0) call die                                        &
    ("Can't allocate space for variables and constants",.false.)
nconst=nval
vname=""
vtype=0
value=0d0
vname(0)="Zero"
value(nval)=1d0
vname(nval)='Unity'

END SUBROUTINE alloc_variables

SUBROUTINE vardefs(v)

!  (VARIABLES) [LIST [ALL | NEW | NONE]]
!  name [=] value [unit]
!  ...
!  END

IMPLICIT NONE
LOGICAL, INTENT(IN) :: v
INTEGER :: i, k, n, nvar0=0, nconst0
CHARACTER(LEN=20) :: ww
LOGICAL :: end, listv=.true.

nconst0=nval
!  List defined variables? (default all)
listv = .true.
do while (item < nitems)
  call reada(ww)
  select case(upcase(ww))
  case("LIST")
    call readu(ww)
    select case(ww)
    case("ALL", "")
      nvar0 = 0
      nconst0 = nval
    case("NEW")
      nvar0 = nvar
      nconst0 = nconst
    case("NONE")
      listv = .false.
    end select
  end select
end do

!  Read variable or constant definitions
do
  call read_line(end)
  if (end) call die('Unexpected end of input',.false.)
  if (nitems .eq. 0) cycle
  call readu(ww)
  if (ww .eq. 'END') exit
  if (ww .eq. 'NOTE' .or. ww .eq. "!") cycle
  call reread(-1)
  call reada(ww)
  k=0
  if (v) then
    !  Defining variable
    do i=1,nvar
      if (ww .eq. vname(i)) k=i
    end do
    if (k .ne. 0) then
      n=k
    else
      nvar=nvar+1
      if (nvar .ge. nconst) then
        call die ("Too many variables or constants",.true.)
      endif
      n=nvar
      vtype(n)=0
    endif
  else
    !  Defining constant
    do i=nconst,nval
      if (ww .eq. vname(i)) k=i
    end do
    if (k .ne. 0) then
      n=k
      print '(1x,2a)', 'Warning: redefining constant ', ww
    else
      nconst=nconst-1
      if (nvar .ge. nconst) then
        call die ("Too many variables or constants",.true.)
      endif
      n=nconst
      vtype(n)=0
    endif
  endif
  vname(n)=ww
  call reada(ww)
  if (ww .ne. "=") call reread(-1)
  call readf(value(n))
  vtype(n)=1
  if (item .lt. nitems) then
    call readu(ww)
    select case(ww)
    case("A","ANGSTROM")
      value(n)=value(n)/bohrva
      vtype(n)=1
    case("D","DEG","DEGREE","DEGREES")
      value(n)=value(n)*pi/180d0
      vtype(n)=2
    case("B","BOHR")
      vtype(n)=1
    case("R","RADIAN","RAD")
      vtype(n)=2
    case("N")
      vtype(n)=0
    case default
      call die('Unit '//trim(ww)//' not recognised.',.true.)
    end select
  else
    value(n)=value(n)/rfact
  endif
end do

if (listv) then
  if (v) then
    if (nvar > nvar0) then
      call vprint('Variables:', nvar0+1,nvar, .false.)
    end if
  else
    if (nconst < nconst0) then
      call vprint('Constants:',nconst0-1,nconst, .false.)
    end if
  end if
end if

END SUBROUTINE vardefs

!=======================================================================

SUBROUTINE readv(index,itype,varble)

!  Read a name (possibly preceded by a minus sign) or a value from the
!  input, and return an index to the element in the array VALUE that
!  holds the value. ITYPE is 1 on input if the quantity is known to be a
!  distance, 2 if it is known to be an angle, 0 otherwise. In the latter
!  case the value is treated as dimensionless.

!  VARBLE is set to .TRUE. if the item read is a variable name, and is
!  unchanged otherwise.

!  If VARBLE is .true. on input and the item is not recognised as a
!  variable name, it is treated as an anonymous variable rather than
!  a constant.

INTEGER, INTENT(OUT) :: index
INTEGER, INTENT(IN) :: itype
LOGICAL :: varble
INTEGER :: is, i
DOUBLE PRECISION :: x

CHARACTER*20 :: w

call reada(w)
if (w(1:1) .eq. '-') then
  is=-1
  w=w(2:)
else
  is=1
endif
!  Named variable?
do i=1,nvar
  if (w .eq. vname(i)) then
    if (itype .eq. vtype(i) .or. itype .eq. 0) then
      index=i*is
      varble=.true.
      return
    else
      write (string,"(4a)") trim(vartype(vtype(i))), &
          " variable used for ", trim(vartype(itype)), " value"
      call die(string,.true.)
    end if
  endif
end do
!  Named constant?
do i=nconst,nval
  if (w .eq. vname(i) .and. itype .eq. vtype(i)) then
    index=i*is
    return
  endif
end do
!  Literal numerical value -- treat as anonymous variable
call reread(-1)
!  Set soft errors
call input_options(error_flag=2)
if (itype .eq. 2) then
  call readf(x,afact)
else if (itype .eq. 1) then
  call readf(x,rfact)
else
  call readf(x)
endif
if (input_error_flag<0) then
  print "(/a/a/a)", "Error while attempting to read a real number.",    &
      "Note that variables must be declared before they are used in",  &
      "molecule or site definitions."
  call die ("",.true.)
else
  !  Reset hard errors
  call input_options(error_flag=0)
end if
i=vslot(.true.)
vname(i)=""
value(i)=x
vtype(i)=itype
index=i

END SUBROUTINE readv

!-----------------------------------------------------------------

INTEGER FUNCTION vslot(variable)

!  Find an unused slot in the VALUE array for a new variable or constant.

LOGICAL variable

if (variable) then
  nvar=nvar+1
  if (nvar .ge. nconst) call die                              &
      ('Not enough space for geometrical parameters',.true.)
  vslot=nvar
else
  !  If possible, re-use a freed variable
  if (freek .gt. 0) then
    vslot=freek
    freek=vtype(freek)
  else
    nconst=nconst-1
    if (nconst .le. nvar) call die                              &
        ('Not enough space for geometrical parameters',.true.)
    vslot=nconst
  endif
endif

END FUNCTION vslot

!-----------------------------------------------------------------

DOUBLE PRECISION FUNCTION vvalue(index)

INTEGER, INTENT(IN) :: index

if (index .lt. 0) then
  vvalue=-value(-index)
else
  vvalue=value(index)
endif

END FUNCTION vvalue

!-----------------------------------------------------------------

SUBROUTINE setval(x,n,type)

!  Set the value of constant N to X, and its type to TYPE, unless
!  that is zero in which case the type is unchanged.

INTEGER, INTENT(IN) :: type
DOUBLE PRECISION, INTENT(IN) :: x
INTEGER :: n

if (n .eq. 0) then
  nconst=nconst-1
  if (nconst .le. nvar) call die                                &
      ('Insufficient space for constant values',.FALSE.)
  n=nconst
  vtype(n)=0
endif
if (n .gt. 0) then
  value(n)=x
else
  value(-n)=-x
endif
if (type .gt. 0) vtype(abs(n))=type

END SUBROUTINE setval

!-----------------------------------------------------------------

SUBROUTINE vprint(c,i1,i2,reinput)

USE output


!  Print the names (if any) and values of the parameters I1 to I2.
!  If I1 is greater than I2, then the list is printed backwards from
!  I1 and the entries are numbered so that the entry at NVAL is 1,
!  the entry at NVAL-1 is 2, and so on.

!  C is a character-string heading.

CHARACTER*(*), INTENT(IN) :: c
INTEGER, INTENT(IN) :: i1, i2
CHARACTER*2 tag
DOUBLE PRECISION fact
INTEGER i, istep, k
LOGICAL reinput

if (i1 .le. i2) then
  istep=1
else
  istep=-1
endif
call putout(1)
call putstr(c,0)
call putout(0)
k=1
do i=i1,i2,istep
  if (vname(i) .ne. ' ') then
    if (vtype(i) .eq. 0) then
      tag=' '
      fact=1d0
    else if (vtype(i) .eq. 1) then
      fact=rfact
      if (rfact .eq. 1d0) then
        tag=' B'
      else
        tag=' A'
      endif
    else
      fact=afact
      tag=' D'
    endif

    if (reinput) then
      print "(2X,A,F12.6,A)", vname(i), vvalue(i)*fact, tag
    else
      call puttab(k)
      if (istep .eq. 1) then
        call puti(i,'(i5)',.false.)
      else
        call puti(nval-i+1,'(i5)',.false.)
      endif
      call putsp(2)
      call putstr(vname(i),0)
      call putstr(' =',1)
      call putg(vvalue(i)*fact,10,6,.true.)
      call putstr(tag,0)
      k=k+25
      if (k .gt. 70) k=1
    endif
  endif
end do
call putout(0)

END SUBROUTINE vprint

!------------------------------------------------------------------

SUBROUTINE vout(i)
USE output

INTEGER i, k
DOUBLE PRECISION fact

call putsp(1)
if (i .eq. 0) then
  call putstr('0.0',0)
else
  k=abs(i)
  if (k .le. nvar .and. vname(k) .ne. "") then
    if (i < 0) call putstr('-',0)
    call putstr(vname(k),0)
  else if (k .le. nvar .or. (k .ge. nconst .and. k .le. nval)) then
    if (vtype(k) .eq. 2) then
      fact=afact
    else if (vtype(k) .eq. 1) then
      fact=rfact
    else
      fact=1d0
    endif
    call putg(fact*vvalue(k),12,6,.true.)
  endif
endif

END SUBROUTINE vout

!------------------------------------------------------------------

SUBROUTINE vary (x)
USE switches
USE molecules
USE properties, ONLY: calclinks
USE input, ONLY : item, nitems


!  Read a variable name and a value from the input, and set the variable
!  to the specified value if X = 1, or increment it by the value if X = 0.

CHARACTER word*12, u*8
LOGICAL newvar
INTEGER i, m
DOUBLE PRECISION v, x

newvar=.false.
do while (item .lt. nitems)
  call reada(word)
  do i=1,nvar
    if (word .eq. vname(i)) then
      newvar=.true.
      exit
    endif
  end do
  if (.not. newvar) then
    do i=nconst,nval
      if (word .eq. vname(i)) call die                                 &
          ('Attempt to change value of constant '//trim(word),.true.)
    end do
    call die('Variable '//trim(word)//' not recognised.',.true.)
  end if

  call reada(u)
  if (u .ne. '=') call reread(-1)
  call readf(v)
  if (item<nitems) then
    if ( vtype(i) == 1) then
      call readu(u)
      select case(u)
      case("A","ANGSTROM")
        v=v/bohrva
      case("B","BOHR")
      case default
        call reread(-1)
        v=v/rfact
      end select
    else if ( vtype(i) == 2) then
      call readu(u)
      select case(u)
      case("D","DEGREE","DEG")
        v=v/afact
      case("R","RADIAN","RAD")
      case default
        call reread(-1)
        v=v/afact
      end select
    end if
  else
    if (vtype(i) == 1) then
      v=v/rfact
    else if (vtype(i) == 2) then
      v=v/afact
    end if
  end if
  value(i)=x*value(i)+v
end do
call vprint('Variables:', 1,nvar, .false.)
do m=1,mols
  call axes(m,.false.)
end do
call calclinks(force=.true.)

END SUBROUTINE vary

!------------------------------------------------------------------

SUBROUTINE findv(name,index)

!  Find and return the index of the named variable, if it exists;
!  otherwise return zero.

CHARACTER(LEN=20), INTENT(IN) :: name
INTEGER, INTENT(OUT) :: index

do index=1,nvar
  if (vname(index) .eq. name) return
end do
index=0

END SUBROUTINE findv

!------------------------------------------------------------------

SUBROUTINE define_variables

USE rotations, ONLY: read_rotation

CHARACTER(LEN=16) :: ww
! INTEGER :: type
LOGICAL :: eof
TYPE(variable), POINTER :: thisvar=>null()
DOUBLE PRECISION :: dummy(3,3)

call reada(ww)
if (upcase(ww).eq."NEW") then
  thisvar=>vlist
  do while (associated(thisvar))
    vlist=thisvar%next
    deallocate(thisvar)
    thisvar=vlist
  end do
  nv=0
end if
do
  call read_line(eof)
  if (eof) call die('Unexpected end of input',.false.)
  call reada(ww)
  select case(upcase(ww))
  case("","!","NOTE")
    cycle
  case("END")
    exit
  case default
    !  New variable
    allocate (thisvar)
    if (.not. associated(vlist)) then
      vlist=>thisvar
    else
      lastvar%next=>thisvar
    end if
    lastvar=>thisvar
    thisvar%name=ww
    thisvar%index=nv+1
    call reada(ww)
    select case(upcase(ww))
    case("LENGTH","L")
      thisvar%type=1
      call readf(thisvar%x,rfact)
    case("ROTATION")
      thisvar%type=3
      call read_rotation(thisvar%p,dummy)
    case default
      !  Length assumed
      thisvar%type=1
      call reread(-1)
      call readf(thisvar%x,rfact)
    end select
    nv=nv+thisvar%type
  end select
end do

call set_variables(.true.)

END SUBROUTINE define_variables

!------------------------------------------------------------------

SUBROUTINE set_variables(print)

LOGICAL, INTENT(IN) :: print
TYPE(variable), POINTER :: thisvar=>null()
INTEGER :: n

if (.not. associated(vlist)) then
  print "(a)", "No variables defined"
  return
end if
if (allocated(x0)) deallocate(x0)
allocate(x0(nv))
x0=0d0

if (print) print "(a)", "Variables:"
thisvar=>vlist
n=0
do while (associated(thisvar))
  if (thisvar%type==1) then
    n=n+1
    x0(n)=thisvar%x
    if (print) print "(i3,1x,2a,F12.7)", n, thisvar%name, " length    ", thisvar%x
  else
    n=n+1
    x0(n:n+2)=thisvar%p
    if (print) print "(i3,1x,2a,3F12.7)", n, thisvar%name, " rotation  ", thisvar%p
    n=n+2
  end if
  thisvar=>thisvar%next
end do

END SUBROUTINE set_variables

END MODULE variables
