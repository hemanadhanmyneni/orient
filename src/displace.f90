SUBROUTINE displace(dx)

!  INPUT :
!     dx(nhess)  : ndim displacements (translation and rotation)
!  OUTPUT :
!     module sites  : new geometry in SX and local axes in SE

!  Remarks :
!  [1] Organization of the displacement vector is (RB =rigid body)
!        (transl RB 1 | rot RB 1 | transl RB 2 | rot RB 2 |....)
!  [2] The arrays "se" and "sx" (in module "sites") are manipulated
!      directly in this subroutine.

USE sites
USE molecules
IMPLICIT NONE

DOUBLE PRECISION, INTENT(IN) :: dx(1:6*mols)
DOUBLE PRECISION :: rotate(3,3), old(3,3), a(3), origin(3)
INTEGER :: base, i, j, ii, k, m1

! print "(a/(6f10.6))", "DX vector: ", dx(1:6*mols)
base=0
do m1=1,mols
!  base=6*(m1-1)
  call draai(dx(base+4:base+6),rotate)
  ! print "(a,i0,a/(3f10.6))", "Molecule ", m1, ": Rotation matrix", &
  !     (rotate(i,:), i=1,3)
  origin=sx(:,cm(m1))
  k=cm(m1)
  do while (k .ne. 0)
    !  Rotate sites around molecular centre of mass
    a=sx(:,k)-origin
    !  Add back origin position + origin shift dx
    do j=1,3
      sx(j,k)=origin(j)+dx(base+j)
      do i=1,3
        sx(j,k)=sx(j,k)+rotate(j,i)*a(i)
      end do
    end do
!  Apply rotation to site orientation matrix, unless it is referred to some
!  other site.
    if (k .eq. sm(k)) then
      old=se(:,:,k)
      se(:,:,k)=0d0
      do j=1,3
        do i=1,3
          do ii=1,3
            se(j,i,k)=se(j,i,k)+rotate(j,ii)*old(ii,i)
          end do
        end do
      end do
    endif
    k=next(k)
  end do     ! end loop over sites for this molecule
  base=base+6
end do     ! end loop over molecules

END SUBROUTINE displace
!----------------------------------------------------------------------------------
SUBROUTINE draai(axis,rotate)

!  Sign convention: Positive rotations are such as to advance a
!         (right handed) corkscrew along the direction of "axis".
!  Goldstein would call this "counterclockwise"

!  axis(3): rotation axis; magnitude gives rotation angle in radians
!  rotate:  rotation matrix

IMPLICIT NONE

DOUBLE PRECISION, INTENT(IN) :: axis(3)
DOUBLE PRECISION, INTENT(OUT) :: rotate(3,3)

DOUBLE PRECISION :: x, y, z, xx, yy, zz, xy, xz, yz, psi, s, s2
INTEGER :: i

x=axis(1)
y=axis(2)
z=axis(3)

psi=sqrt(x*x+y*y+z*z)
if (psi .lt. 1d-13) then
  rotate=0d0
  do i=1,3
    rotate(i,i)=1d0
  end do
else
  x=x/psi
  y=y/psi
  z=z/psi
  xx=2d0*x*x
  xy=2d0*x*y
  xz=2d0*x*z
  yy=2d0*y*y
  yz=2d0*y*z
  zz=2d0*z*z
  s=sin(psi)
  s2=(sin(psi/2d0))**2

  rotate(1,1)= 1d0 - (yy+zz)*s2
  rotate(1,2)= -z*s + xy*s2
  rotate(1,3)= y*s + xz*s2
  
  rotate(2,1)= z*s + xy*s2
  rotate(2,2)= 1d0 - (xx+zz)*s2
  rotate(2,3)= -x*s+ yz*s2

  rotate(3,1)= -y*s + xz*s2
  rotate(3,2)= x*s + yz*s2
  rotate(3,3)= 1d0 - (xx+yy)*s2
endif

END SUBROUTINE draai
