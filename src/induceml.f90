SUBROUTINE induceml(order,maxit,eind)

!  Calculate the induction energy of the system
!  and the derivatives to order `order'.

!  maxit is the maximum number of induction iterations to be performed. If it
!  is zero, or switch(8)=.false., only the first-order induced moments are
!  calculated.

USE alphas
USE consts
USE damping
USE indparams
USE induction
USE molecules
USE monolayer
USE Sfns, ONLY : evals
USE sites
USE sizes
USE switches


IMPLICIT NONE

INTEGER, INTENT(IN) :: order, maxit
DOUBLE PRECISION, INTENT(OUT) :: eind

INTEGER :: adin, adin2, i, i1,k1, k2, m, m1, m2, n, p, pa, pk1, pk2,   &
    t1, z1, z2, iterations
DOUBLE PRECISION :: eindm, dqsq
LOGICAL :: converged, polarizable
DOUBLE PRECISION, PARAMETER :: tol=1D-6, half=0.5d0

if (.not. switch(9) .or. .not. switch(8))                              &
    call cleardq(.false.,dqsq)

!  Calculate induced moments

iterations = 0
dqsq = 1d0
converged = .false.

do while (iterations .le. maxit .and. .not. converged)
  !  We begin with the first-order induced moments.
  !  Loop over polarizable sites

  do adin = 0,nlayers
    if (nmolls(adin) > 0) then
      m = layer(adin)
      do while (m .ne. 0)
        k1 = firstp(m)
        !  write(*,*)' pk1', pk1
        do while (k1 .gt. 0)
          pk1 = ps(k1)
          z1 = pz(k1)
          k2 = firstp(m)
          !  write(*,*)' pk2', pk2
          do while (k2 .gt. 0)
            pk2 = ps(k2)
            pa = ixa(k1,k2)
            !  write(*,*)' pa', pa
            if (pa > 0) then
              z2 = pz(k2)
              !  write(*,*)' z',z1,z2
              do i = 1,z1
                dq0(i,pk1,1) = dq0(i,pk1,1)                             &
                    - dot_product(alpha(i,1:z2,pa),v0(1:z2,pk2))
              end do
            endif
            k2 = nextp(k2)
          end do           ! pk2
          k1 = nextp(k1)
        end do             ! pk1
        m = nextm(m)
      end do ! m .ne. 0
    end if ! nmolls(adin) .gt. 0
  end do ! adin

  if (iterations > 0 .and. maxit > 0) then
    !  Calculate contributions induced by induced moments
    !  Loop over molecules
    n = 1
    do adin = 0,nlayers
      if (nmolls(adin) > 0) then
        m1 = layer(adin)
        do while (m1 .ne. 0)
          ! Need to ensure that cluster (layer(0)) is NOT taken to be
          ! periodic. Best way seems to loop over index adin2=1,nlayers
          ! because adin2=0 has no meaning...
          do adin2 = 1,nlayers
            if (nmolls(adin2) > 0) then
              m2 = layer(adin2)
              do while (m2 .ne. 0)
                k1 = firstp(m1)
                !  write(*,*)'pk1',pk1
                do while (k1 > 0)
                  pk1 = ps(k1)
                  k2 = firstp(m2)
                  !  write(*,*)'pk2',pk2
                  do while (k2 > 0)
                    pk2 = ps(k2)
                    !  Loop over unit cells in the layer
                    !  (Cell (0,0) excluded)
                    do p = 1,mlcells
                      if (m1 .ne. m2) then
                        call mlinfo(m1,k1,m2,k2,order,                      &
                            mlindex(1,p),mlindex(2,p),adin2)
                        call listinds(k1,k2,n)
                        call evals(order,n)
                        call deltaqml(m1,k1,m2,k2,n)
                      endif
                    enddo
                    k2 = nextp(k2)
                  end do         ! pk2
                  k1 = nextp(k1)
                end do           ! pk1
                m2 = nextm(m2)
              end do  ! m2 .ne. 0
            end if  ! nmolls(adin2) .gt. 0
          end do  ! adin2
          m1 = nextm(m1)
        end do ! m1 .ne. 0
      end if ! nmolls(adin) .gt. 0
    end do ! m1
  endif

  !  Copy induced moment array dqn(..,1) to dqn(..,0), accumulating squared
  !  changes to induced moments
  call cleardq(.true.,dqsq)
  converged = (dqsq < tol .or. .not. switch(8) .or. maxit == 0)

  eind = 0d0
  do m = 1,mols
    eindm = 0d0
    polarizable = .false.
    k1 = firstp(m)
    do while (k1 .gt. 0)
      pk1 = ps(k1)
      polarizable = .true.
      !  if (print .ge. 6) then
      !  z1 = pz(k1)
      !  print '(A,I3/A,F14.8)',
      !  &        'Induced moments on site', k1,
      !  &            'Charge        ', dq0(1,pk1,0)
      !  if (z1 .gt. 1) print '(A,3F14.8)',
      !  &          'Dipole        ', (dq0(t1,pk1,0), t1=2,min(4,z1))
      !  if (z1 .gt. 4) print '(A,5F14.8)',
      !  &          'Quadrupole    ', (dq0(t1,pk1,0), t1=5,min(9,z1))
      !  endif
      if (print .ge. 8) then
        print '(A/(2I2, 6F12.8))', 'Derivatives of moments',            &
            ((t1, m1, (dq1(i1,m1,t1,pk1,0), i1=1,6),                    &
            m1=1,mols), t1=1,z1)
      endif
      eindm = eindm + dot_product(dq0(1:z1,pk1,0),v0(1:z1,pk1))
      !  write(*,*)'e',(t1,pk1,dq0(t1,pk1,0),v0(t1,pk1), t1=1,z1)
      k1 = nextp(k1)
    end do
    eindm = half*eindm
    if (polarizable) then
      if (print >= 2 .and. (converged .or.                              &
          iterations == 0 .or. iterations == maxit))                    &
          print '(a,i2,a,f15.8,1x,a)',                                  &
          'Induction energy of molecule', m, ': ', eindm*efact, eunit
      eind = eind + eindm
    endif
  end do

  !  print '(A,F15.8)',
  !  &      'Sum of squares of changes to induced moments: ', dqsq
  if (print .gt. 6) print '(A,F15.11)',                                 &
      'Sum of squares of changes to induced moments:    ', dqsq
  !         '                             first derivatives:  ', dq1sq, &
  !         '                             second derivatives: ', dq2sq

  if (print >= 2 .and. (converged .or.                                &
      iterations == 0 .or. iterations == maxit)) then
    do m = 1,mols
      k1=firstp(m)
      do while (k1 > 0)
        pk1 = ps(k1)
        z1 = pz(k1)
        !  print '(A,I3/A,F14.8)', 'Induced moments on site', k1,
        !  &          'Charge        ', dq0(1,pk1,0)
        !  if (z1 .gt. 1) print '(A,3F14.8)',
        !  &          'Dipole        ', (dq0(t1,pk1,0), t1=2,min(4,z1))
        !  if (z1 .gt. 4) print '(A,5F14.8)',
        !  &          'Quadrupole    ', (dq0(t1,pk1,0), t1=5,min(9,z1))
        k1 = nextp(k1)
      end do
    end do
  endif
  iterations = iterations + 1
end do  !  end of iteration loop

!  print '(A)', 'Derivatives including induction'
!  do m1=1,mols
!  print '(3F20.10)', (deriv1(i1,m1), i1=1,6)
!  end do

END SUBROUTINE induceml

!-----------------------------------------------------------------------

SUBROUTINE deltaqml(m1,k1,m2,k2,n0)

!  Calculate induced moments for induction energy if required. Here we
!  just calculate the contribution to the induced moment deltaQ at pk1 (pk2)
!  from the induced moments at pk2 (pk1).

!  Note that the first-order induced moments must be calculated elsewhere,
!  outside the loop over site pairs. We assume that it has been done
!  before this routine is called.

USE indparams
USE alphas, ONLY : alpha, nextp, ixa, npol
USE consts
USE damping
USE induction, ONLY : dq0
USE interact
USE molecules
USE pairderiv
USE Sfns, ONLY : t1s, t2s
USE sites
USE sizes
USE switches
USE types, ONLY : indform, indampf, pairindex

IMPLICIT NONE

INTEGER, INTENT(IN) :: k1, k2, m1, m2, n0

INTEGER :: k, ix, n, pa, t1, t2, t1a, k1a, pk2, pk1a, z1, z2
DOUBLE PRECISION :: vr(7), aa, qq, g0, rkd, f0, f1, f2
LOGICAL :: newd, newk

pk2 = ps(k2)
z1 = pz(k1)
z2 = pz(k2)

vr(1)=1d0/r
vr(2)=vr(1)**2

!  Damped or undamped r dependence for T functions
!  using Tang-Toennies or other damping functions.
t1 = type(k1)
t2 = type(k2)
if (t1 > 0 .and. t2 > 0) then
  pair=pairindex(t1,t2)
else
  pair=0
endif

!  Loop over electrostatic interactions
k = 1
! fk = 1d0
newd = .true.
newk = .true.
n = n0
do while(n == n0 .or. list(n) > 1)
  ix = list(n)
  t1 = t1s(ix)
  t2 = t2s(ix)
  !  write(*,*)'o',t1,t2,z1,z2
  if (t1 .le. z1 .or. t2 .le. z2) then
    do while (k < power(n))
      k = k + 1
      vr(k) = vr(k-1)*vr(1)
      newk = .true.
    end do
    if (newk) then
      !  Evaluate induction damping function f0 and its derivatives f1 and f2
      ! fk=k
      call damp(indform(pair),indampf(0:,pair),k,alpha0,newd, r, f0,f1,f2)
      newd = .false.
      !  rkd is the damped R^-k and rkd1 and rkd2 are its derivatives w.r.t. R
      !  g0 is the damped T function and g1 and g2 are its derivatives w.r.t.
      !  external variables
      rkd = vr(k)*f0
      newk = .false.
    endif
    g0 = fac(n)*rkd*s0(n)
  endif

  !  Moments induced in the sites of molecule m1 by fields at k1. Here
  !  we calculate only the contribution from the induced moments at k2,
  !  not the static moments.
  if (t1 .le. z1) then
    k1a = firstp(m1)
    do while (k1a > 0)
      pk1a = ps(k1a)
      pa = ixa(k1,k1a)
      if (pa > 0) then
        do t1a = 1,pz(k1a)
          aa = alpha(t1,t1a,pa)
          if (aa .ne. 0d0) then
            qq = dq0(t2,pk2,0)*aa
            dq0(t1a,pk1a,1) = dq0(t1a,pk1a,1) - qq*g0
            !  write(*,*)'p',aa,dq0(t2,pk2,0),dq0(t1a,pk1a,1)
          endif
        end do
      endif
      k1a = nextp(k1a)
    end do
  endif
  n = n + 1
end do    ! end of loop over electrostatic functions

END SUBROUTINE deltaqml

!-----------------------------------------------------------------------

SUBROUTINE cleardq(update,dqsq)

USE indparams
USE induction
USE alphas, ONLY: npol

IMPLICIT NONE
LOGICAL, INTENT(IN) :: update
DOUBLE PRECISION, INTENT(OUT) :: dqsq
INTEGER p

dqsq=0d0
if (update) then
  !  Copy n=1 induced moments to n=0
  do p=1,npol
    dqsq=dot_product((dq0(:,p,0)-dq0(:,p,1)),(dq0(:,p,0)-dq0(:,p,1)))
    dq0(:,p,0)=dq0(:,p,1)
  end do
else
  !  Clear n=0 induced moments to zero
  do p=1,npol
    dq0(:,p,0)=0d0
  end do
endif

!  Clear n=1 induced moments
do p=1,npol
    dq0(:,p,1)=0d0
end do

END SUBROUTINE cleardq
