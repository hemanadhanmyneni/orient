SUBROUTINE printq(q,n,fmt)

IMPLICIT NONE
DOUBLE PRECISION, INTENT(IN) :: q(*)
INTEGER, INTENT(IN) :: n
CHARACTER(LEN=*), INTENT(IN) :: fmt

if (n .ge. 0) print fmt, 'Charge      ', q(1)
if (n .ge. 1) print fmt, 'Dipole      ', q(2:4)
if (n .ge. 2) print fmt, 'Quadrupole  ', q(5:9)
if (n .ge. 3) print fmt, 'Octopole    ', q(10:16)
if (n .ge. 4) print fmt, 'Hexadecapole', q(17:25)

END SUBROUTINE printq
