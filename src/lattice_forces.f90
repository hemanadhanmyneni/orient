SUBROUTINE lattice_forces(order, m,esm)

!  Evaluate the electrostatic energy between molecule m and the lattice
!  using reciprocal-space summation (Lambin & Senet)
!  order=1 for first derivative, 2 for 2nd derivatives, but derivatives
!  are not currently implemented

USE parameters
USE indparams
USE consts
USE molecules
USE lattice
USE rotations, ONLY : wigner
USE sites
USE sizes
USE switches

IMPLICIT NONE

INTEGER, INTENT(IN) :: order, m
DOUBLE PRECISION, INTENT(OUT) :: esm
DOUBLE PRECISION :: d(sq,sq),qg(sq),s(SQ)
INTEGER :: i, j, k, n, ppo, p, pk1,pol

esm=0d0
k=head(m)
do while (k .gt. 0)
  if (type(k) .gt. 0 .or. l(k) .ge. 0) then
    p=qsize(l(k))
    !  write(*,*)'k,lk',k,l(k)
    !  Transform moments to global axes (not necessary if l<1)
    if (l(k) .gt. 0) then
      call wigner(se(1,1,sm(k)), d, l(k))
      call dgemv("N", p, p, 1d0, d,SQ, q(1,k),1, 0d0, qg,1)
    else
      qg(1)=q(1,k)
    endif
    !  Evaluate energy and derivatives w.r.t. c.m. position
    if (l(k) .ge. 0) then
      call lattice_sums(order, sx(1,k), l(k), s,k)
      do n=1,p
        esm=esm+s(n)*qg(n)
        !     write(*,*)'hej',esm,n,s(n),qg(n)
      end do
    endif
  endif
  k=next(k)
end do

END SUBROUTINE lattice_forces
