MODULE sites

USE parameters
USE input, ONLY : die

INTEGER, SAVE :: nsites, tops, free, ldef=-1
CHARACTER*20, SAVE, ALLOCATABLE :: name(:)
INTEGER, DIMENSION(:), SAVE, ALLOCATABLE :: next, l, link, level,      &
    st, sf, sm, sw, type, ps, pz, pr
INTEGER, SAVE, ALLOCATABLE :: sp(:,:), sr(:,:), fragment(:)
DOUBLE PRECISION, SAVE, ALLOCATABLE :: sx(:,:), se(:,:,:), q(:,:)
DOUBLE PRECISION, ALLOCATABLE, SAVE :: slink(:,:),srotlink(:,:,:)

!  NSITES is the maximum number of sites that can be used.
!  NEXT is the next site in the chain describing a particular molecule;
!    zero for the last site in the molecule.
!  TOPS is the top site so far defined.
!  FREE is a pointer to a chain of free sites (zero unless sites have
!       been deleted).
!  LDEF is the default maximum rank for multipoles.

!  It would be sensible to define a site type with the following elements,
!  but that would involve extensive changes throughout the program.

!  L is the rank of moments on this site.
!  LINK points back to the parent site of this one.
!  LEVEL is the number of links between this site and the global axis
!    system; i.e. 1 for the root site, 2 for sites defined relative to that
!    (the default), 3,4, etc. for sites defined relative to other sites.
!  SP(:,k) is a set of three pointers to the variables (or constants)
!    describing the position of this site.
!  SR(:,k) is a set of three or four pointers giving the orientation of this
!    site. If there are three, they point to the Euler angles alpha, beta,
!    gamma; if four, they are the angle of rotation and the components of the
!    vector in the direction of the rotation axis.
!  ST(k) is a pointer to the twist angle.
!  SF(k) is an integer flag whose bits have the following significance:
!    bit 0    1  Rotation described by Euler angles;
!    bit 1    2  Rotation described by angle/axis variables;
!    bit 2    4  There is a twist about the bond to the parent site;
!    bit 3    8  The rotation and/or twist is variable, not constant;
!    bit 4   16  Position is described by polar coordinates;
!    bit 5   32  Position is described by cartesian coordinates;
!    bit 6   64  Position is variable.
!  SM points to the rotation matrix that describes the orientation of this
!    site.
!  SX gives the position of this site as last evaluated, in global coordinates.
!  SE gives the rotation matrix as last evaluated, unless SM points to
!    the rotation matrix for some other site.  SE(i,j,SM(K)) is the direction
!    cosine between global axis i and local axis j for site K.
!  Note that during a geometry optimization, SX and SE do not correspond
!  to the positions and orientations as defined by SP and SR. The variables
!  to which these pointers refer are updated at the end of the optimization
!  (whether it converges or not).
!  Q is the set of multipole moments for this site.
!  TYPE is the type of this site.
!  PR(k), if non-negative, indicates that this site is polarizable, and
!  specifies the rank of polarizabilities on this site.
!  PZ(K) is the number of polarizable components to be included for
!  site k -- initially qsize(pr(k)) for a polarizable site but can be
!  reduced by a LIMIT command.
!  PS(k) if nonzero, points to the entry in the arrays of fields and
!  induced moments for site k.

!  SW is an integer work-space variable, available for use by subroutines.
!  NAME is the name of the site.

!  Site 0 is reserved for properties of the system as a whole (global centre
!  of mass, etc.)
!  Sites -2 and -1 are reserved for use in Ewald summations.

!  The following arrays are constructed by the calclinks routine:
!  slink(:,k) contains the position of site k relative to the centre of mass
!  of the molecule.
!  srotlink(:,:,k) contains the orientation of site k relative to the inertial
!  axes of the molecule.


CONTAINS

SUBROUTINE alloc_sites
IMPLICIT NONE
INTEGER :: ok, i

allocate (next(-2:nsites), l(-2:nsites), link(-2:nsites),               &
    level(-2:nsites), st(-2:nsites), sf(-2:nsites), sm(-2:nsites),      &
    sp(3,-2:nsites), sr(4,-2:nsites), sw(-2:nsites), fragment(nsites),  &
    type(-2:nsites), name(-2:nsites), ps(nsites), pz(nsites), pr(nsites), &
    sx(3,-2:nsites), se(3,3,-2:nsites), q(sq,-2:nsites), stat=ok)
if (ok .gt. 0) call die                                                 &
    ("Couldn't allocate arrays in SITES module",.true.)

next(:)=0
l(:)=0
link(:)=0
level(:)=0
st(:)=0
sf(:)=0
sm(:)=0
sp(:,:)=0
sr(:,:)=0
sw(:)=0
type(:)=0
name(:)=""
sx(:,:)=0d0
se(:,:,:)=0d0
q(:,:)=0d0
ps = 0
pr = -1
pz = 0
!  Global axes
do i=1,3
  se(i,i,0)=1
end do

END SUBROUTINE alloc_sites

SUBROUTINE new_site(k)
IMPLICIT NONE
INTEGER, INTENT(OUT) :: k
INTEGER :: i

if (free .gt. 0) then
  k=free
  free=next(free)
else
  tops=tops+1
  k=tops
  if (k .gt. nsites) call die("Can't allocate site",.true.)
endif
sm(k)=k
se(:,:,k)=0d0
do i=1,3
  se(i,i,k)=1d0
end do

END SUBROUTINE new_site

SUBROUTINE read_site(k,m,warn)

!  Read a name from the input and find a site in molecule M with that
!  name. If there is more than one, the last site defined is returned.
!  There is an error if no site of that name is found, except that if
!  the name read is null, or if the optional logical variable warn is
!  present and .true., zero is returned.

USE input, ONLY : reada
IMPLICIT NONE

INTEGER, INTENT(IN) :: m
LOGICAL, INTENT(IN), OPTIONAL :: warn
INTEGER, INTENT(OUT) :: k
CHARACTER(20) :: w

k=0
call reada(w)
if (w .eq. ' ') return
k=find_site(m,w,warn)

END SUBROUTINE read_site

INTEGER FUNCTION find_site(m,w,warn)

USE molecules, ONLY : head

!  Find a site in molecule M with name w.
!  If there is more than one, the last site defined is returned.
!  There is an error if no site of that name is found, except that if
!  the name read is null, or if the optional logical variable warn is
!  present and .true., zero is returned.

INTEGER, INTENT(IN) :: m
CHARACTER(LEN=*), INTENT(IN) :: w
LOGICAL, INTENT(IN), OPTIONAL :: warn
INTEGER :: i

find_site=0
i=head(m)
do while ( i .ne. 0 )
  if (w .eq. name(i)) find_site=i
  i=next(i)
end do

if (find_site == 0) then
  if (present(warn)) then
    if (warn) return
  end if
  call die ('Site '//trim(w)//' not found',.true.)
end if

END FUNCTION find_site

INTEGER FUNCTION locate(w,warn)

!  Find the molecule named w. If the molecule is not found, return 0 if
!  warn is present and true, otherwise print an error message and stop.

USE molecules, ONLY: head, mols
USE input, only : die
IMPLICIT NONE
CHARACTER(LEN=*), INTENT(IN) :: w
LOGICAL, INTENT(IN), OPTIONAL :: warn

INTEGER i

do i=1,mols
  if (name(head(i)) == w) then
    locate=i
    return
  endif
end do
if (present(warn)) then
  if (warn) then
    locate=0
    return
  end if
end if
call die("Can't find molecule "//trim(w),.true.)

END FUNCTION locate

END MODULE sites
